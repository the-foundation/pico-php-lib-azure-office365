<?php
/*
 *   Mail script - ORIG FROM :  Katy Nicholson, April 2020https://katystech.blog/2020/08/php-graph-mailer/
 *
 *  sends messages from an Exchange Online mailbox using MS Graph API
 *
 *
 */
class graphMailer
{
    var $Token;
    var $baseURL;
    function __construct($sToken)
    {
        $this->baseURL = "https://graph.microsoft.com/v1.0/";
        //$this->Token = $getTokenFunctionName();
        $this->Token = $sToken;
    }
    // function __construct($sTenantID, $sClientID, $sClientSecret) {
    //     $this->tenantID = $sTenantID;
    //     $this->clientID = $sClientID;
    //     $this->clientSecret = $sClientSecret;
    //     $this->baseURL = 'https://graph.microsoft.com/v1.0/';
    //     $this->Token = $this->getToken();
    // }
    //
    // function getToken() {
    //     $oauthRequest = 'client_id=' . $this->clientID . '&scope=https%3A%2F%2Fgraph.microsoft.com%2F.default&client_secret=' . $this->clientSecret . '&grant_type=client_credentials';
    //     $reply = $this->sendPostRequest('https://login.microsoftonline.com/' . $this->tenantID . '/oauth2/v2.0/token', $oauthRequest);
    //     $reply = json_decode($reply['data']);
    //     return $reply->access_token;
    // }
    //
    function getMessages($mailbox)
    {
        if (!$this->Token) {
            throw new Exception("No token defined");
        }
        $messageList = json_decode(
            $this->sendGetRequest(
                $this->baseURL .
                    "users/" .
                    $mailbox .
                    "/mailFolders/Inbox/Messages"
            )
        );
        if ($messageList->error) {
            throw new Exception(
                $messageList->error->code . " " . $messageList->error->message
            );
        }
        $messageArray = [];

        foreach ($messageList->value as $mailItem) {
            $attachments = json_decode(
                $this->sendGetRequest(
                    $this->baseURL .
                        "users/" .
                        $mailbox .
                        "/messages/" .
                        $mailItem->id .
                        "/attachments"
                )
            )->value;
            if (count($attachments) < 1) {
                unset($attachments);
            }
            foreach ($attachments as $attachment) {
                if (
                    $attachment->{'@odata.type'} ==
                    "#microsoft.graph.referenceAttachment"
                ) {
                    $attachment->contentBytes = base64_encode(
                        "This is a link to a SharePoint online file, not yet supported"
                    );
                    $attachment->isInline = 0;
                }
            }
            $messageArray[] = [
                "id" => $mailItem->id,
                "sentDateTime" => $mailItem->sentDateTime,
                "subject" => $mailItem->subject,
                "bodyPreview" => $mailItem->bodyPreview,
                "importance" => $mailItem->importance,
                "conversationId" => $mailItem->conversationId,
                "isRead" => $mailItem->isRead,
                "body" => $mailItem->body,
                "sender" => $mailItem->sender,
                "toRecipients" => $mailItem->toRecipients,
                "ccRecipients" => $mailItem->ccRecipients,
                "toRecipientsBasic" => $this->basicAddress(
                    $mailItem->toRecipients
                ),
                "ccRecipientsBasic" => $this->basicAddress(
                    $mailItem->ccRecipients
                ),
                "replyTo" => $mailItem->replyTo,
                "attachments" => $attachments,
            ];
        }
        return $messageArray;
    }

    function deleteEmail($mailbox, $id, $moveToDeletedItems = true)
    {
        switch ($moveToDeletedItems) {
            case true:
                $this->sendPostRequest(
                    $this->baseURL .
                        "users/" .
                        $mailbox .
                        "/messages/" .
                        $id .
                        "/move",
                    '{ "destinationId": "deleteditems" }',
                    ["Content-type: application/json"]
                );
                break;
            case false:
                $this->sendDeleteRequest(
                    $this->baseURL . "users/" . $mailbox . "/messages/" . $id
                );
                break;
        }
    }

    function sendMail($mailbox, $messageArgs)
    {

        $dumpArgs=false;
        if (Settings::$debugAppVerbose   || Settings::$debugApp|| Settings::$debugAppMail ) { $dumpArgs=true;  }
        if (!$this->Token) {
            throw new Exception("No token defined");
        }
        if($dumpArgs) {
            print_r(htmlentities(json_encode($messageArgs,JSON_PRETTY_PRINT), ENT_QUOTES));

        }
        /*
        $messageArgs[   subject,
                replyTo{'name', 'address'},
                toRecipients[]{'name', 'address'},
                ccRecipients[]{'name', 'address'},
                importance,
                conversationId,
                body,
                images[],
                attachments[]
                ]
        */
        $messageArray = [];
        if (
            isset($messageArgs["saveToSentItems"]) &&
            $messageArgs["saveToSentItems"]
        ) {
            $messageArray["saveToSentItems"] = true;
        }


        if(!isset($messageArgs["toRecipients"])) { return array ( "status"=>"FAIL","reason"=>"PARAM_MISSING_toRecipients","result"=>array("status"=>"FAIL","reason"=>"PARAM_MISSING_toRecipients") ); ;}
        foreach ($messageArgs["toRecipients"] as $recipient) {
			// catch recpients was given as array of strings and not array of arrays
			if(is_array($recipient) && isset($recipient["address"])) { 
				$curaddr=$recipient["address"];
				} else { 
					$curaddr=$recipient;					
			}
            if (is_array($recipient) && isset($recipient["name"])) {
                $messageArray["toRecipients"][] = [
                    "emailAddress" => [
                        "name" => $recipient["name"],
                        "address" => $curaddr,
                    ],
                ];
            } else {
				
                $messageArray["toRecipients"][] = [
                    "emailAddress" => ["address" => $curaddr ],
                ];
            }
        }
        if (!isset($messageArgs["ccRecipients"])) {
            $messageArgs["ccRecipients"] = [];
        }

        foreach ($messageArgs["ccRecipients"] as $recipient) {
            if ($recipient["name"]) {
                $messageArray["ccRecipients"][] = [
                    "emailAddress" => [
                        "name" => $recipient["name"],
                        "address" => $recipient["address"],
                    ],
                ];
            } else {
                $messageArray["ccRecipients"][] = [
                    "emailAddress" => ["address" => $recipient["address"]],
                ];
            }
        }
        if(!isset($messageArgs["subject"])) { return array ( "status"=>"FAIL","reason"=>"PARAM_MISSING_subject","result"=>array("status"=>"FAIL","reason"=>"PARAM_MISSING_toRecipients") );}
        $messageArray["subject"] = $messageArgs["subject"];
        
        if(isset($messageArgs["importance"])) {
        $messageArray["importance"] = $messageArgs["importance"] ? $messageArgs["importance"] : "normal";
            } else { $messageArray["importance"]="normal" ; }
        
        if (isset($messageArgs["replyTo"])) {
            $messageArray["replyTo"] = [
                [
                    "emailAddress" => [
                        "name" => $messageArgs["replyTo"]["name"],
                        "address" => $messageArgs["replyTo"]["address"],
                    ],
                ],
            ];
        }
        if(!isset($messageArgs["body"])) { return array ( "status"=>"FAIL","reason"=>"PARAM_MISSING_body","result"=>array("status"=>"FAIL","reason"=>"PARAM_MISSING_body") );}

        $messageArray["body"] = [
            "contentType" => "HTML",
            "content" => $messageArgs["body"],
        ];
        
        ///// NOT DONE YET

        ///        $response = json_decode($response['data']);
        ///        $messageID = $response->id;
        ///
        /////        foreach ($messageArgs['images'] as $image) {
        /////            $messageJSON = json_encode(array('@odata.type' => '#microsoft.graph.fileAttachment', 'name' => $image['Name'], 'contentBytes' => base64_encode($image['Content']), 'contentType' => $image['ContentType'], 'isInline' => true, 'contentId' => $image['ContentID']));
        /////            $response = $this->sendPostRequest($this->baseURL . 'users/' . $mailbox . '/messages/' . $messageID . '/attachments', $messageJSON, array('Content-type: application/json'));
        /////        }
        /////
        /////        foreach ($messageArgs['attachments'] as $attachment) {
        /////            $messageJSON = json_encode(array('@odata.type' => '#microsoft.graph.fileAttachment', 'name' => $attachment['Name'], 'contentBytes' => base64_encode($attachment['Content']), 'contentType' => $attachment['ContentType'], 'isInline' => false));
        /////            $response = $this->sendPostRequest($this->baseURL . 'users/' . $mailbox . '/messages/' . $messageID . '/attachments', $messageJSON, array('Content-type: application/json'));
        /////        }

        ///// NOT DONE YET

        //var_dump($messageArray)   ;
        $msg = [];
        $msg["message"] = $messageArray;
        // $messageJSON = json_encode($msg);
        $messageJSON = json_encode($msg, JSON_PRETTY_PRINT);
        // FOR MULTISTAGE:       $response = $this->sendPostRequest($this->baseURL . 'users/' . $mailbox . '/messages', $messageJSON, array('Content-type: application/json'));

        //var_dump($msg);
        //var_dump($messageJSON);
		 if (Settings::$debugAppVerbose ||   Settings::$debugAppMail ) {
           echo "\n sending graph mail..";           //var_dump([$to, $subject, base64_encode($message), $headers]);
         }
        //Send
        // $response = $this->sendPostRequest($this->baseURL . 'users/' . $mailbox . 'sendMail', $messageJSON, array('Content-type: application/json'));
        $payload = $messageJSON;
        $token = AuthHelper::getDaemonToken();
        $targetURL =
            Settings::$unifiedAPIEndpoint . "users/" . $mailbox . "/sendMail";

        //	 var_dump($targetURL);
        $request = curl_init($targetURL);
        curl_setopt($request, CURLOPT_HTTPHEADER, [
            "Authorization: Bearer " . $token->accessToken,
            "Accept: application/json",
            "Content-Type:application/json",
            //  "Content-Type: text/plain"
        ]);

        curl_setopt($request, CURLOPT_POST, true);
        curl_setopt($request, CURLOPT_POSTFIELDS, $payload);
        curl_setopt($request, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($request);
        //echo '<script>console.log(\' RES: '.$response.'\');</script>';
        //parse the json into oci_fetch_object
        //$result = json_decode($response, true);
        //return $result;
        //var_dump($response);
		 if (Settings::$debugAppVerbose ||   Settings::$debugAppMail ) {
           echo "\n sent curl graph mail..RESP_CODE".curl_getinfo($request, CURLINFO_HTTP_CODE) ;           //var_dump([$to, $subject, base64_encode($message), $headers]);
         }      

        ///var_dump(json_decode($response));

        if (curl_getinfo($request, CURLINFO_HTTP_CODE) == "202") {
			//;
			return array ( "status"=>"OK","result"=>json_decode($response, true) );
        }
        return array ( "status"=>"FAIL","reason"=>"HTTP_CODE_NOT_202","result"=>json_decode($response, true) );
    }

    function basicAddress($addresses)
    {
        foreach ($addresses as $address) {
            $ret[] = $address->emailAddress->address;
        }
        return $ret;
    }

    function sendDeleteRequest($URL)
    {
        $ch = curl_init($URL);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            "Authorization: Bearer " . $this->Token,
            "Content-Type: application/json",
        ]);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close($ch);
        echo $response;
    }

    function sendPostRequest($URL, $Fields, $Headers = false)
    {
        $ch = curl_init($URL);
        curl_setopt($ch, CURLOPT_POST, 1);
        if ($Fields) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $Fields);
        }
        if ($Headers) {
            $Headers[] = "Authorization: Bearer " . $this->Token;
            curl_setopt($ch, CURLOPT_HTTPHEADER, $Headers);
        }
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        $responseCode = curl_getinfo($ch, CURLINFO_RESPONSE_CODE);
        curl_close($ch);
        return ["code" => $responseCode, "data" => $response];
    }

    function sendGetRequest($URL)
    {
        $ch = curl_init($URL);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            "Authorization: Bearer " . $this->Token,
            "Content-Type: application/json",
        ]);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close($ch);
        return $response;
    }
}
