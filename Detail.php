<?php
$start_time = microtime(true);

require_once("Settings.php");
if (Settings::$debugApp) {

  error_reporting(E_ALL|E_STRICT);
  ini_set("display_errors", 1);
}

session_start();

//require_once("AuthHelper.php");
//require_once("Token.php");
require_once("MS-graph-functions.php");


//check for token in session first time in
if (!isset($_SESSION[Settings::$tokenCache])) {
  //redirect to login page
  header("Location:Login.php");
}
else {
  //check for id and apiRoot
  if (!isset(Settings::$apiRoot) || !isset($_GET["id"]) ) {
    //redirect back to Index.php
    header("Location:Index.php");
  }
  else {
    //get addin value
    $isaddin = Settings::$isAddin;

    //get the apiRoot from session
    $apiRoot =       $targetURL=Settings::$unifiedAPIEndpoint ;


    //get the id from url parameter
    $id = $_GET["id"];

    //use the refresh token to get a new/cached access token
    $token = AuthHelper::getDaemonToken();

    // preliminary group info fetch
    //perform a REST query for GROUP itself

    $request = curl_init($apiRoot . "/groups/" . $id );
//var_dump($apiRoot . "/groups/" . $id );
    curl_setopt($request, CURLOPT_HTTPHEADER, array(
      "Authorization: Bearer " . $token->accessToken,
      "Accept: application/json"));
    curl_setopt($request, CURLOPT_RETURNTRANSFER, true);
    $response = curl_exec($request);
    $grpJSON = json_decode($response, true);
//var_dump($response);


  $displayName=$grpJSON["displayName"];
  $grpDesc=$grpJSON["description"];
  $grpMail=$grpJSON["mail"];
  $grpDate=$grpJSON["renewedDateTime"];
  $grpID=$grpJSON["id"];

$members=graph_getGroupByOID($id);


    echo '<html>
<head>
  <title>'.Settings::$AppName.'- GROUP DETAIL</title>
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <script type="text/javascript" src="scripts/jquery-1.10.2.min.js"></script>
  <script type="text/javascript" src="scripts/bootstrap.min.js"></script>';
echo '<script>console.log(\' RESP:'.$response.'\');</script>
<style>';
   require_once("style.php");
  echo '</style>';


/// if (Settings::$isAddin == true) { echo '
///  <script type="text/javascript" src="//appsforoffice.microsoft.com/lib/1/hosted/office.js"></script>
///  <script type="text/javascript">
///  //initialize Office on each page if add-in
///  Office.initialize = function(reason) {
///    $(document).ready(function() {
///      var data = JSON.parse(excelData);
///      var officeTable = new Office.TableData();
///
///      //build headers
///      var headers = new Array("Name", "Email", "Job Title", "Department");
///      officeTable.headers = headers;
///
///      //add data
///      for (var i = 0; i < data.value.length; i++) {
///        officeTable.rows.push([data.value[i].displayName,
///          data.value[i].mail,
///          data.value[i].jobTitle,
///          data.value[i].department]);
///      }
///
///      //add the table to Excel
///      Office.context.document.setSelectedDataAsync(officeTable, { coercionType: Office.CoercionType.Table }, function (asyncResult) {
///        //check for error
///        if (asyncResult.status == Office.AsyncResultStatus.Failed) {
///          $("#error").show();
///        }
///        else {
///          $("#success").show();
///        }
///      });
///    });
///  }
///
///  var excelData = \'';echo $response ;'\';
///  </script>';
///}

  }
}



echo '</head><body>  <div class="maincontainer min85 center" >';

echo '<div class="row min85 brightbg round center min85">';
require_once("Menu.php");
echo '<div class="col-sm-12">
        <div class="alert alert-success" role="alert" style="display: none;" id="success">
          SUCCESS: Update to Excel succeeded!
        </div>
        <div class="alert alert-danger" role="alert" style="display: none;" id="error">
          ERROR: Update to Excel failed!
        </div></div>';

echo '<div class="row min85 center"><center><table style="border:2px solid;min-width: 55%;"  ><tbody><tr>
<td colspan=2><center><h1> GROUP DETAILS <br></h1></center></td>
</tr><tr>

<td ><h2>Name: </h2></td><td><h2> [ '.$displayName.' ]  </h2></td>

</tr><tr>
<td> Description: </td><td style="border:1px solid;" > '.$grpDesc.' </td>
</tr><tr>
</tr><tr>
<td> Member Count: </td><td style="border:1px solid;" > '.count($members["value"]).' </td>
</tr><tr>

<td style="border:1px solid;"  > id: </td><td> '.$grpID.' </td>
</tr><tr>
<td> mail ( list addr): </td><td style="border:1px solid; "> '.$grpMail.' </td>
</tr><tr>
<td style="border:1px solid; " > renewedDateTime : </td><td > '.$grpDate.' </td>
</tr>
</tbody>
</table></center>
</div>
<hr>
<div class="row min85 center">
<div  style="border:1px solid;"><center><h1> MEMBERS:</h1></center>';

echo '       <table class="table table-striped table-hover">
          <thead>
            <tr>
              <th>Name</th>
              <th>Email</th>
              <th>Title</th>
              <th>Principal</th>
            </tr>
          </thead>
          <tbody>';

          foreach ($members["value"] as $member) {
			    echo '<script>console.log(\' MEMBER:'.json_encode($member).'\');</script>';
			  echo '<tr>
                    <td>'.$member["displayName"].'</td>
                <td><a href="mailto:'.$member["mail"].'" >'.$member["mail"].'</a></td>
                <td>'.$member["jobTitle"].'</td>
                <td>'.$member["userPrincipalName"].'</td>
              </tr>';
		  }
echo '
          </tbody>
        </table>
      </div>
    </div>
  </div>
 </div>' ;
echo '<hr> <div class="round border1 brightbg" id="renderTime" >  <center>Render time: '.( microtime(true) - $start_time).' seconds   </center></div>';

echo' </body>
</html>' ;

?>
