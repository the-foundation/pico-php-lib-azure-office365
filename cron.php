<?php

$start_time = microtime(true);

require_once("Settings.php");
if (Settings::$debugApp) {

  error_reporting(E_ALL|E_STRICT);
  ini_set("display_errors", 1);
}

session_start();

//require_once("AuthHelper.php");
//require_once("Token.php");
require_once("MS_graph_functions.php");

//$users = graph_getAllUsers();
$mininterval=120;
// with 1000 users == 1000 images + 1 master + 1 guest + 50 group requests plus group add/del (31650 with 2 minutes)
// you quickly reach the 10k req/h Limit from Graph



$status="OK";
$redis = new Redis();
$redis->connect(Settings::$redis_host, Settings::$redis_port);

$usecache=true;
$timing=array();

$relative_start=microtime(true);
graph_CacheUsersFromArray(graph_getAllUsers($usecache));
$timing["getUsers"]=microtime(true)-$relative_start;

$relative_start=microtime(true);
graph_getAllGroups();
$timing["getGroups"]=microtime(true)-$relative_start;

$relative_start=microtime(true);
graph_CacheGuestsFromArray(graph_getGuests($usecache));
$timing["CacheGuests"]=microtime(true)-$relative_start;

$relative_start=microtime(true);
graph_getAllProfilePictures();
$timing["CacheImages"]=microtime(true)-$relative_start;

//foreach ( $redis->keys('*_api_user_principa*') as $key )  {
//  print_r($key);
//}

// ONLY DO THIS IN CLI SINCE WE ASSUME THAT WE RUN JOBS LONGER THAN INTERVAL
if(isset(Settings::$CronFile) && php_sapi_name() == "cli") {

   require_once(Settings::$CronFile);




}

//print(json_encode(array(
//    "status" => $status,
//    "render_time" => ( microtime(true) - $start_time),
// )));
$statusArray=array("status" => $status , "timings" => $timing);

if( php_sapi_name() != "cli" ) {
  print(json_encode($statusArray,true));
}
?>
