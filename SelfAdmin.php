<?php
$start_time = microtime(true);


if (!class_exists("Settings"))	 {
  	require_once( dirname(__FILE__) . "/Settings.php");

  }
if (Settings::$debugApp) {

  error_reporting(E_ALL|E_STRICT);
  ini_set("display_errors", 1);
}
if(php_sapi_name() != "cli") {
	session_start();
	}

//require_once("AuthHelper.php");
//require_once("Token.php");
require_once("MS-graph-functions.php");


function graph_caching_headers_redis ($content,$cache_key,$redis) {
    $ttl=$redis->ttl($cache_key);
    if($ttl<2){ $ttl = 0; }
    $lastModified=time()-Settings::$cache_time_oid+$ttl;
    $timestamp=$lastModified;
    //$lastModified=filemtime($_SERVER['SCRIPT_FILENAME']);
    $gmt_mtime = gmdate("D, d M Y H:i:s T", $lastModified);
    header('REDIS-TTL: "'.$ttl.'"');
    header('ETag: "'.md5($timestamp.md5($cache_key)).'"');
    header('Last-Modified: '.$gmt_mtime);
    header('Cache-Control: must-revalidate, proxy-revalidate, max-age=3600');
    if(isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) || isset($_SERVER['HTTP_IF_NONE_MATCH'])) {
        if ($_SERVER['HTTP_IF_MODIFIED_SINCE'] == $gmt_mtime || str_replace('"', '', stripslashes($_SERVER['HTTP_IF_NONE_MATCH'])) == md5($timestamp.md5($cache_key))) {
            header('HTTP/1.1 304 Not Modified');
            header("Vary: Accept-Encoding,User-Agent");
            exit();
        }
    }
}


if ( php_sapi_name() != "cli" && $_SERVER['REQUEST_METHOD'] === 'POST' )  {
    // do post
    $paramsOK=false;
    if (verifyInternalToken("adminaction",$_POST["token"] ) ) {
		    if(isset($_POST["SelfAction"])) {
		    //things we sent to our self via post
        if($_POST["SelfAction"]=="createUser") {
			  //create user via Portal ( auto adding domain )

			if( ( !empty($_POST["fname"]) && !empty($_POST["lname"]) ) ) {
				//principal gen first


				if( isset($_POST["principal"]) && !empty($_POST["principal"]) ) {
				   // only first and lastname given , join fname/lname without specials
	            // WE ONLY ACCEPT CHARS and - .
				   $mailname=$_POST["principal"];
                 } else {
					 // no principal specified via POST
					 $_POST["principal"] = preg_replace("/[^a-zA-Z0-9\-\.]+/", "", $_POST["fname"]."-".$_POST["lname"]  );
					 $mailname=$_POST["principal"];

				 } // end principal set and not empry

				// WE ONLY ACCEPT CHARS. EXCLAMATION MARK
				$_POST["fname"] = preg_replace("/[^a-zA-Z0-9]+/", "", $_POST["fname"]);
				$_POST["lname"] = preg_replace("/[^a-zA-Z0-9]+/", "", $_POST["lname"]);


                $paramsOK=true;
				echo "## Creating User ".$_POST["principal"]."@".Settings::$tenantDOMAIN." with random password ## \n <br>\n";

				echo "<code>";
				$createresult=graph_CreateUser($_POST["fname"].' '.$_POST["lname"],$mailname,$_POST["principal"]."@".Settings::$tenantDOMAIN,uniqid("Def").md5(uniqid() ) ,$_POST["fname"],$_POST["lname"] );

				echo str_replace("\n", "\n<br>",

				                             json_encode($createresult, JSON_PRETTY_PRINT )
				                              );
				echo "</code>";
			}// end POST have first and lastname
		} // end SelfAtion createuser

        if($_POST["SelfAction"]=="inviteGuest") {
			//invite user via Portal ( auto adding domain )

			if(  !empty($_POST["fname"]) && !empty($_POST["lname"])  && !empty($_POST["mail"]) )  {
				// WE ONLY ACCEPT CHARS. EXCLAMATION MARK
				$_POST["fname"] = preg_replace("/[^a-zA-Z0-9]+/", "", $_POST["fname"]);
				$_POST["lname"] = preg_replace("/[^a-zA-Z0-9]+/", "", $_POST["lname"]);
				if( !isset($_POST["principal"]) || empty($_POST["principal"]) ) {
				   // no principal aka  only first and lastname given , join fname/lname without specials
				   $_POST["principal"] = strtolower(preg_replace("/[^a-zA-Z0-9]+/", "", $_POST["mail"]  ));
                    }
                  if(!verifyMailAddressViaDNS($_POST["mail"])) {
					print_r("SOFTFAIL, could not find one of [ MX | A | AAAA ] DNS Records for users Domain  ");
				  }
		        $displayName=$_POST["fname"].' '.$_POST["lname"];

		        $msgbody= preg_replace("/_MYDOMAIN_/", Settings::$tenantDOMAIN, Settings::$inviteMailBody );
		        $msgbody= preg_replace("/_DISPLAYNAME_/", $displayName,$msgbody );

				 print "## Inviting guest User ".$_POST["principal"]."@".Settings::$tenantDOMAIN." ##\n";
				echo "<code>";
				$paramsOK=true;
        				print_r(str_replace("\n", "\n<br>", json_encode( graph_InviteUserSendCustomMail(true,
                                                                                          $displayName,
                                                                                          $_POST["mail"],
                                                                                          Settings::$inviteRedirURL,
                                                                                          "Guest",
                                                                                          preg_replace("/_MYDOMAIN_/", Settings::$tenantDOMAIN, Settings::$inviteMailSubject ),
                                                                                          $msgbody,
                                                                                          Settings::$inviteMailFooter,
                                                                                          strtolower(Settings::$tenantINVITEmail)."@".strtolower(Settings::$tenantDOMAIN),"graph_API"), JSON_PRETTY_PRINT )));

        				     //if($fname!=false)    {
        		             //   $userRequest["givenName"] = $fname;
        		             //}
                             //if($lname!=false)    {
        		             // $userRequest["surname"] = $lname;
        		             //}
				echo "</code>";
            } // end  lname + fname + mail set
           } // end invite Guest








         if($_POST["SelfAction"]=="createGroup") {
			//cretate user via Portal ( auto adding domain )

			if(  isset($_POST["gname"]) && !empty($_POST["gname"]) )  {
			  // WE ONLY ACCEPT CHARS and - .
			  $_POST["gname"] = preg_replace("/[^a-zA-Z0-9\-\.]+/", "", $_POST["gname"]);

			  if( ( !empty($_POST["displayName"]) && !empty($_POST["displayName"]) ) ) {
				  $displayName=$_POST["displayName"];
			  } else {
				  // no displayname , default to gname
				  $displayName=$_POST["gname"];
				  }
			if(  isset($_POST["ownerOID"]) && !empty($_POST["ownerOID"]) )  {
				  $ownerOID=$_POST["ownerOID"];
		      } else { //no owner given
				  $ownerOID=graph_getOID_byMail_OR_Principal(strtolower(Settings::$tenantADMINmail).'@'.strtolower(Settings::$tenantDOMAIN));
			  }

				$securityEnabled=true;
				$paramsOK=true;
					print(json_encode(graph_CreateGroup($_POST["gname"],$ownerOID, $displayName,$displayName,$securityEnabled), JSON_PRETTY_PRINT ))	;


			}
		  if( false == $paramsOK) {
		     print "\nYOUR PARAMS ARE NOT OK SOMEHOW\n\n";print_r(json_encode($_POST,JSON_PRETTY_PRINT)); return false;
	        }
		} // end create POST selfAction=createGroup
    if($_POST["SelfAction"]=="DeleteGuestUsersWithNoGroupMembership") {
                $paramsOK=true;
                $msg="Deleting Guest Users With No Group Membership \nTHIS CAN BE A TIME_INTENSIVE PROCESS .. PLEASE KEEP THIS PAGE OPENED: ..\n\n RESULTS BELOW..\n---\n\n";
                 if(php_sapi_name() != "cli") {  echo str_replace("\n", "<br>",$msg) ; } else { print $msg; }
                while(ob_get_level() > 0) {  ob_end_clean(); }
                print " ";while(ob_get_level() > 0) {  ob_end_clean(); }
                //self-explaining
                print_r(str_replace("\n", "<br>",
                   htmlentities(
                      addslashes(
                      json_encode(graph_DeleteGuestUsersWithNoGroupMembership(),JSON_PRETTY_PRINT)
                        ), ENT_QUOTES)
                    )
                );
                }
	    if( false == $paramsOK) {
		     print "\nYOUR PARAMS ARE NOT OK SOMEHOW\n\n";print_r(json_encode($_POST,JSON_PRETTY_PRINT)); return false;
	    }
	  } // end POST selfAction
	 // end user is verified via adminaction token
	   } else { // end user has no token
				//unauthorized
				http_response_code(403);//unauthorized
				exit;
				}
} else  { //end POST
    // do get


if(isset($_GET) && isset($_GET["GETSelfAction"])) {
  //user is authorized and sent us something to do via GET ( web , not CLI )
  if (!isset($_SESSION[Settings::$tokenCache])) {
          return json_encode(array("status" => "FAIL" , "reason" =>"UNAUTHORIZED"));

   } else  {
      if($_GET["GETSelfAction"] == "getImageByOID_RAW" &&  isset($_GET["oid"] ) ) {
            header('Expires: '.gmdate('D, d M Y H:i:s \G\M\T', time() + (60 * 60))); // 1 hour
            header('Content-Type: application/json; charset=utf-8');
            print(json_encode(graph_getImagebyOID($_GET["oid"]),true ) );
          } //end getImageRAW
      if($_GET["GETSelfAction"] == "getImageContentByOID" &&  isset($_GET["oid"] ) ) {
        //will redirect to cached image or just default person.svg
            //header('Content-Type: application/json; charset=utf-8');
            //print(json_encode(graph_getImagebyOID($_GET["oid"]),true ) );
            $res=graph_getImageCacheKeyByOID($_GET["oid"]);
            if(is_string($res)  && $res != "") {
                header('Location: '.$_SERVER['PHP_SELF'].'?GETSelfAction=getImageContentByCheckSum&checkSUM='.$res );
                //print("REDIRECTING TO ".$_SERVER['PHP_SELF'].'?GETSelfAction=getImageByCheckSum&checkSUM='.$res);
            } else {
                header('Location: /assets/person.svg' );
            }

          } //end getImage

      if($_GET["GETSelfAction"] == "getImageSumByOID" &&  isset($_GET["oid"] ) ) {
            //header('Content-Type: application/json; charset=utf-8');
            //print(json_encode(graph_getImagebyOID($_GET["oid"]),true ) );
            $res=graph_getImageCacheKeyByOID($_GET["oid"]);
            if(is_string($res)  && $res != "") {
                header('Content-Type: application/json; charset=utf-8');
                print(json_encode(array("keysum" => $res  ,"oid" => $_GET["oid"] ) ));
                //header('Location: '.$_SERVER['PHP_SELF'].'?GETSelfAction=getImageContentByCheckSum&checkSUM='.$res );
                //print("REDIRECTING TO ".$_SERVER['PHP_SELF'].'?GETSelfAction=getImageByCheckSum&checkSUM='.$res);
            } else {
              //var_dump($res);

              //http_response_code(409);//conflict
              print(json_encode(array("keysum" => "" ,"oid" => $_GET["oid"] ) ));
              exit;
            }

          } //end getImage

      if($_GET["GETSelfAction"] == "getImageContentByCheckSum" &&  isset($_GET["checkSUM"] ) ) {
            //$etag=$_GET["checkSUM"];
            if(!isset($redis)) { $redis = new Redis(); $redis->connect(Settings::$redis_host, Settings::$redis_port); }
            $cache_key="ms_graph_api_image_".$_GET["checkSUM"];
            $res=graph_getImageByCheckSum($_GET["checkSUM"]);
            if(isset($res["status"]) && $res["status"] == "OK"  ) {
              graph_caching_headers_redis($res["result"],$cache_key,$redis);
              if(isset($_GET["base64"] ) && $_GET["base64"]== true ) {
                header('Content-Type: data/base64');
                print($res["result"]);
              } else {
                header('Content-Type: image/jpeg');
                print(base64_decode($res["result"])  );
              }






            ////  //if (isset($_SERVER['HTTP_IF_NONE_MATCH']) &&  $_SERVER['HTTP_IF_NONE_MATCH'] == $etag) {
            ////  //header('res: '.addslashes(json_encode($res)));
            ////  //ob_start(); // collect all outputs in a buffer



            ////  //$sContent = ob_get_contents(); // collect all outputs in a variable
            ////  //ob_clean();
            ////  $sEtag=md5($res["result"]);
            ////  //if(isset($_SESSION["etag_".$_GET["checkSUM"]]) &&  $_SESSION["etag_".$_GET["checkSUM"]]==md5($res["result"]) ) {
            ////  if ($_SERVER['HTTP_IF_NONE_MATCH'] == $sEtag) { //browser already requested this page ?
            ////    header('HTTP/1.1 304 Not Modified');
            ////       header_remove("Cache-Control");
            ////       header_remove("Pragma");
            ////       header_remove("Expires");
            ////    //header('', true, 304);
            ////    //http_response_code(304);
            ////    exit;
            ////  } else {
            ////    header('Etag: "'.$sEtag.'"');	// send a ETag with the response

            ////    header('Expires: '.gmdate('D, d M Y H:i:s \G\M\T', time() + (60 * 60))); // 1 hour

            ////    header('Pragma: cache');
            ////    header('Cache-Control: public, must-revalidate, max-age=3600');
            ////    header('Content-Type: image/jpeg');

            ////    print(base64_decode($res["result"])  );


            ////   //header_remove("Cache-Control"); //let the browser cache the content
            ////   //header_remove("Pragma");
            ////   //header_remove("Expires");
            ////   //echo $sContent;
            ////   //$_SESSION["etag_".$_GET["checkSUM"]]=md5($res["result"]);
            ////   exit;
            ////  }


            } else {
              header('Expires: '.gmdate('D, d M Y H:i:s \G\M\T', time() + 1)); // 1 second
              header('Location: /assets/person.svg' );
              //http_response_code(302);//redir
              exit;
             }
             http_response_code(402);//conflict
          } //end getImageByCheckSum
      if($_GET["GETSelfAction"] == "getImageByCheckSum" &&  isset($_GET["checkSUM"] ) ) {

            $res=graph_getImageByCheckSum($_GET["checkSUM"]);
            if(isset($res["status"]) && $res["status"] == "OK"  ) {
               header('Expires: '.gmdate('D, d M Y H:i:s \G\M\T', time() + (60 * 60))); // 1 hour
               header('Content-Type: application/json; charset=utf-8');
               print(json_encode($res)  );
            }
          } //end getImageByCheckSum

   }

} else {
//user gets the page
if (!isset($_SESSION[Settings::$tokenCache])) {
  //redirect to login page
  header("Location:/Login.php?redirect=/SelfAdmin.php");
  print("UNAUTHORIZED");
  die(" UNAUTHORIZED - PLEASE LOGIN ");
}


    //get addin value
     if (isset ($_SESSION[Settings::$isAddin]) && $_SESSION[Settings::$isAddin]) {
    $isaddin = true; } else { $isaddin=false;}
    //get the apiRoot from session
  //  $apiRoot = $_SESSION[Settings::$apiRoot];

    $users = graph_getAllUsers();

  //$displayName=$usrJSON["displayName"];
  //$grpDesc=$usrJSON["description"];
  //$grpMail=$usrJSON["mail"];
  //$grpDate=$usrJSON["renewedDateTime"];
  //$grpID=$usrJSON["id"];

    //$users=json_decode($usrJSON);
    $members = $users["value"];

    echo '<html>
<head>
  <title>'.Settings::$AppName.' - Portal</title>
  <link rel="stylesheet" href="/style.css">
  <link rel="stylesheet" href="css/bootstrap.min.css">

<script>console.log(\' RESP_USERS:'.json_encode($users).'\');</script>

  <script type="text/javascript" src="scripts/jquery-1.10.2.min.js"></script>
  <script type="text/javascript" src="scripts/bootstrap.min.js"></script>
  <style>


th,
td {
	padding: 15px;
	background-color: rgba(255,255,255,0.2);
	/*color: #fff;*/
}

th {
	text-align: center;
}

thead {
	th {
		background-color: #55608f;
	}
}

tbody {
	tr {
		&:hover {
			background-color: rgba(255,255,255,0.3);
		}
	}
	td {
		position: relative;
		&:hover {
			&:before {
				content: "";
				position: absolute;
				left: 0;
				right: 0;
				top: -9999px;
				bottom: -9999px;
				background-color: rgba(255,255,255,0.2);
				z-index: -1;
			}
		}
	}
}

';
require_once(Settings::$styleSheetPHPfile);
echo '</style>';


 if (isset ($isaddin) && $isaddin) { echo '
  <script type="text/javascript" src="//appsforoffice.microsoft.com/lib/1/hosted/office.js"></script>
  <script type="text/javascript">
  //initialize Office on each page if add-in
  Office.initialize = function(reason) {
    $(document).ready(function() {
      var data = JSON.parse(excelData);
      var officeTable = new Office.TableData();

      //build headers
      var headers = new Array("Name", "Email", "Job Title", "Department");
      officeTable.headers = headers;

      //add data
      for (var i = 0; i < data.value.length; i++) {
        officeTable.rows.push([data.value[i].displayName,
          data.value[i].mail,
          data.value[i].jobTitle,
          data.value[i].department]);
      }

      //add the table to Excel
      Office.context.document.setSelectedDataAsync(officeTable, { coercionType: Office.CoercionType.Table }, function (asyncResult) {
        //check for error
        if (asyncResult.status == Office.AsyncResultStatus.Failed) {
          $("#error").show();
        }
        else {
          $("#success").show();
        }
      });
    });
  }

  var excelData = "';echo $response ;'";
  </script>';
}


echo '</head><body style="linear-gradient(45deg, #deddf8, #dabffd);" >   <div><div class="maincontainer" style="min-width: 85%;" >';

echo '<div class="row brightbg min85 center">';
require_once("Menu.php");
echo '</div><div class="col-sm-12">
        <div class="alert alert-success" role="alert" style="display: none;" id="success">
          SUCCESS: Update to Excel succeeded!
        </div>
        <div class="alert alert-danger" role="alert" style="display: none;" id="error">
          ERROR: Update to Excel failed!
        </div></div>';

$resolvedOID=graph_getOID_byMail_OR_Principal(strtolower(Settings::$tenantADMINmail)."@".strtolower(Settings::$tenantDOMAIN));
$defaultOID=(( $resolvedOID == null || $resolvedOID  == "" ) ? "not found" : $resolvedOID  );
$admin_token=setInternalToken("adminaction");
echo '
<hr>
<div class="round row brightbg min85 center">
<br>
<div class="round row brightbg min85 center"><center><table style="border:2px solid;min-width: 20%;"  ><tbody><tr class="round brightbg min85 border1">
<td colspan=2><center><h3> Service Portal</h3></center></td>
</tr><tr class="round brightbg min85 border1">
<td><h4> DOMAIN: </h4></td><td class="round brightbg min85 border1" > <center> default group owner:  ' ;

echo '( <a href="'.graph_LicenseUserSettingsURLFromOID(graph_getOID_byMail_OR_Principal(strtolower(Settings::$tenantINVITEmail)."@".strtolower(Settings::$tenantDOMAIN))).'"><code>'.Settings::$tenantADMINmail."@".Settings::$tenantDOMAIN.'</code><br> <code><b>OID: '.$defaultOID.'  )</b></code></a>   </center></td></tbody>

</table></center>
</div>
<div  class="brightbg min85 center" >';

echo '       <table class="table table-striped table-hover">
<tr>
<td style="min-height: 266 px;width: 42%" >
			<center><form action="/SelfAdmin.php" style="min-height: 333px;width: 99%" target="my-iframe" method="post">

			<table>
<tr><td colspan=2 >
			<center><h2>Invite Guest user </h2></center></td></tr>	<tr><td>
  <label for="fname">First name:</label></td><td>
   <input type="text" name="fname" id="fname"></td><td>
  </td></tr>	<tr><td>
  <label for="lname">Last  name:</label></td><td>
   <input type="text" name="lname" id="lname"></td><td>
   </td></tr>	<tr><td>
  <label for="lname">Mail Address</label></td><td>
   <input type="text" name="mail" id="mail"></td><td>
</td></tr>	<tr><td>
  <center><input type="submit" value="Invite Guest User"></center>
</td></tr></table>
<input type="hidden" id="SelfAction" name="SelfAction" value="inviteGuest">
<input type="hidden" id="token" name="token" value="'.$admin_token.'">
</form></center>


<center><form action="/SelfAdmin.php" style="min-height: 266px;width: 99%" target="my-iframe" method="post">

			<table                          style="min-height: 266px;width: 99%" >
<tr><td colspan=2 >
			<center><h2>Create user on  '.strtolower(Settings::$tenantDOMAIN).'</h2></center></td></tr>	<tr><td>
  <label for="fname">First name:</label></td><td>
   <input type="text" name="fname" id="fname"></td><td>
  </td></tr>	<tr><td>
  <label for="lname">Last  name:</label></td><td>
   <input type="text" name="lname" id="lname"></td><td>
   </td></tr>	<tr><td>
  <label for="lname">Desired address (pare before @)</label></td><td>
   <input type="text" name="principal" id="lname">@'.strtolower(Settings::$tenantDOMAIN).'</td><td>
</td></tr>	<tr><td>
  <center><input type="submit" value="Create DOMAIN User"></center>
</td></tr></table>
<input type="hidden" id="SelfAction" name="SelfAction" value="createUser">
<input type="hidden" id="token" name="token" value="'.$admin_token.'">
</form></center>


<center><form action="/SelfAdmin.php" style="min-height: 222px;width: 99%" target="my-iframe" method="post">

			<table                          style="min-height: 222px;width: 99%"  >
<tr><td colspan=2 >
			<center><h2>Delete group-less Guests on <input type="text" id="bogus" name="bogus" value="'.strtolower(Settings::$tenantDOMAIN).'" class="field left" readonly></h2></center></td></tr>	<tr><td colspan=2>

  <b>DANGER:</b> This will Remove Guest users that are in no Group
   ON '.strtolower(Settings::$tenantDOMAIN).'</td><td>
</td></tr>	<tr><td>
  <center><input type="submit" value="DELETE GUESTS with no GROUP"></center>
</td></tr></table>
<input type="hidden" id="SelfAction" name="SelfAction" value="DeleteGuestUsersWithNoGroupMembership">
<input type="hidden" id="token" name="token" value="'.$admin_token.'">
</form></center>


<center><form action="/SelfAdmin.php" style="min-height: 266x;width: 99%" target="my-iframe" method="post">

			<table                          style="min-height: 266x;width: 99%">
<tr><td colspan=2 >
			<center><h2>Create Group on  '.strtolower(Settings::$tenantDOMAIN).'</h2><h4>NOTE: this might fail ,<br><a href="https://aad.portal.azure.com/#blade/Microsoft_AAD_IAM/GroupsManagementMenuBlade/AllGroups"> Please use the MS Azure Portal</a> </h4></center></td></tr>	<tr><td>

  <label for="fname">Group ID (short name):</label></td><td>
   <input type="text" name="gname" id="gname">@'.strtolower(Settings::$tenantDOMAIN).'</td><td>
   </td></tr><tr><td>
  <label for="lname">Displayed Group Name  </label></td><td>
   <input type="text" name="displayName" id="displayName" value=""></td><td>
</td></tr>	<tr><td>
  Default Group Owner (part before @)</td><td>
   '.strtolower(Settings::$tenantADMINmail).'@'.strtolower(Settings::$tenantDOMAIN).'</td><td>
</td></tr>  <tr><td>
  <center><input type="submit" value="Create GROUP"></center>
</td></tr></table>
<input type="hidden" id="SelfAction" name="SelfAction" value="createGroup">
<input type="hidden" id="token" name="token" value="'.$admin_token.'">
</form></center>

		</td><td>
		<center><h2>Result:</h2></center><br>
<iframe style="height: 600px;width: 99%"  name="my-iframe" src="/assets/logo.png" ></iframe>

</td>

</tr>
';



echo '<tbody></tbody></table>';

///
///echo '       <table class="table table-striped table-hover">
///          <thead>
///            <tr>
///              <th><br>Status</th>
///              <th>Name</th>
///              <th>Email</th>
///              <th>Title</th>
///              <th>Principal|OID</th>
///            </tr>
///          </thead>
///          <tbody>';
///
///          foreach ($members as $member) {
///
///            if(graph_PrincipalIsExternal($member["userPrincipalName"])) {
///                     $membertype="Guest";
///            } else { $membertype="Member"; }
///
///			    echo '<script>console.log(\' MEMBER:'.json_encode($member).'\');</script>';
///			  echo '<tr>
///                <td>'.$membertype.'</td>
///                <td>'.$member["displayName"].'</td>
///                <td><a href="mailto:'.$member["mail"].'" >'.$member["mail"].'</a></td>
///                <td>'.$member["jobTitle"].'</td>
///                <td><code>'.$member["userPrincipalName"].'</code><br><code><b>'.$member["id"].'</b></code></td>
///              </tr>';
///		  }
///echo '
///          </tbody>
///        </table>';
///
echo '    <hr>
        <br>
      </div>
      <hr>
      <br>
    </div>
  </div>
 </div>' ;
echo '<hr> <div class="round border1 brightbg" id="renderTime" >  <center>Render time: '.( microtime(true) - $start_time).' seconds   </center></div>';

echo' </body>
</html>' ;
  } // end else if GETSelfAction
} // end not cli and not POST (e.g. GET)

//if(php_sapi_name() == "cli" && isset($_GET) ) {
//
//
//
//}

?>
