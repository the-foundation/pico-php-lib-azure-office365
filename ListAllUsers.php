<?php
$start_time = microtime(true);


if (!class_exists("Settings"))	 {
  	require_once( dirname(__FILE__) . "/Settings.php");

  }
if (Settings::$debugApp) {

  error_reporting(E_ALL|E_STRICT);
  ini_set("display_errors", 1);
}
if(php_sapi_name() != "cli") {
	session_start();
	}
$syslog="";
//require_once("AuthHelper.php");
//require_once("Token.php");
require_once("MS-graph-functions.php");

//check for token in session first time in
if (!isset($_SESSION[Settings::$tokenCache])) {
  //redirect to login page
  header("Location:Login.php");
  return 0;
}

    //get addin value
     if (isset ($_SESSION[Settings::$isAddin]) && $_SESSION[Settings::$isAddin]) {
    $isaddin = true; } else { $isaddin=false;}
    //get the apiRoot from session
  //  $apiRoot = $_SESSION[Settings::$apiRoot];



    echo '<html>
<head>
  <title>'.Settings::$AppName.' - USER LIST</title>
  <link rel="stylesheet" href="/css/bootstrap.min.css">
  <script>
    var proc_uptime = new Date().getTime();
    function millisToMinutesAndSeconds(millis) {
       var minutes = Math.floor(millis / 60000);
       var seconds = ((millis % 60000) / 1000).toFixed(0);
       return minutes + ":" + (seconds < 10 ? \'0\' : \'\') + seconds;
     }
   </script>
  <script type="text/javascript" src="/scripts/jquery-1.10.2.min.js"></script>

<!-- <script src="//cdn.jsdelivr.net/npm/pouchdb@7.2.1/dist/pouchdb.min.js"></script> -->
  <script type="text/javascript" src="/scripts/pouchdb-7.2.1.min.js"></script>
  <script type="text/javascript" src="/scripts/bootstrap.min.js"></script>

    <style>';
    require_once(Settings::$styleSheetPHPfile);
  echo '</style>';

// get the users here
  $users = graph_getAllUsers(true);
  $guestsraw=graph_getGuests(true);
  $guests=array();
    // now do some indexed array
    if(isset($guestsraw["value"])) {
    foreach ($guestsraw["value"] as $rawguest ) {
      if (isset($rawguest["id"])) {
          $guests[$rawguest["id"]]=$rawguest;
      }
    }
    }



  //$displayName=$usrJSON["displayName"];
  //$grpDesc=$usrJSON["description"];
  //$grpMail=$usrJSON["mail"];
  //$grpDate=$usrJSON["renewedDateTime"];
  //$grpID=$usrJSON["id"];

    //$users=json_decode($usrJSON);
    $members = $users["value"];

  if (Settings::$debugAppVerbose) {
echo "\n ".'<script>console.log(\' RESP:'.json_encode($users).'\');</script>';
echo "\n ".'<script>console.log(\' GUEST_RESP:'.json_encode($guestsraw).'\');</script>';
echo "\n ".'<script>console.log(\' GUESTS_INDEXED:'.json_encode($guests).'\');</script>';
}

 if (isset ($isaddin) && $isaddin) { echo '
  <script type="text/javascript" src="//appsforoffice.microsoft.com/lib/1/hosted/office.js"></script>
  <script type="text/javascript">
  //initialize Office on each page if add-in
  Office.initialize = function(reason) {
    $(document).ready(function() {
      var data = JSON.parse(excelData);
      var officeTable = new Office.TableData();

      //build headers
      var headers = new Array("Name", "Email", "Job Title", "Department");
      officeTable.headers = headers;

      //add data
      for (var i = 0; i < data.value.length; i++) {
        officeTable.rows.push([data.value[i].displayName,
          data.value[i].mail,
          data.value[i].jobTitle,
          data.value[i].department]);
      }

      //add the table to Excel
      Office.context.document.setSelectedDataAsync(officeTable, { coercionType: Office.CoercionType.Table }, function (asyncResult) {
        //check for error
        if (asyncResult.status == Office.AsyncResultStatus.Failed) {
          $("#error").show();
        }
        else {
          $("#success").show();
        }
      });
    });
  }

  var excelData = "';echo $response ;'";
  </script>';
}


echo '</head><body>  <div class="maincontainer  brightbg" style="height:'.(30*count($members)).'px"  >';

echo '<div class="row min85 center" style="   background: rgba(244,244,255,0.80);" >';
require_once("Menu.php");
echo '<div class="col-sm-12">
        <div class="alert alert-success" role="alert" style="display: none;" id="success">
          SUCCESS: Update to Excel succeeded!
        </div>
        <div class="alert alert-danger" role="alert" style="display: none;" id="error">
          ERROR: Update to Excel failed!
        </div></div>';
//get the default group owner OID
$resolvedOID=graph_getOID_byMail_OR_Principal(strtolower(Settings::$tenantADMINmail)."@".strtolower(Settings::$tenantDOMAIN));
$defaultOID=(( $resolvedOID == null || $resolvedOID  == "" ) ? "not found" : $resolvedOID  );

echo '<div class="row min85 center" style="   background: rgba(244,244,255,0.80);" ><center><table style="border:2px solid;min-width: 55%;"  ><tbody><tr>
<td colspan=2><center><h1> ALL USERS <br></h1></center></td>
</tr><tr>
<td><h2> DOMAIN: </h2></td><td style="border:1px solid;" > <center><h3> [ '.Settings::$tenantDOMAIN.' ] </h3> </center></td>


</tr><tr>
<td style="border:1px solid;"> COUNT: </td><td  > '.count($members).' </td>
</tr><tr>
<td style="border:1px solid;"  > default group owner: </td><td> '.Settings::$tenantADMINmail.' ( <a href="mailto:'.Settings::$tenantADMINmail."@".Settings::$tenantDOMAIN.'" > '.Settings::$tenantADMINmail."@".Settings::$tenantDOMAIN.' </a>) </td>

</tr><tr>
<td style="border:1px solid;"  > default group owner OID: </td><td> '.$defaultOID.' </td>


</tr>
</tbody>
</table></center>
</div>
<hr>
<div class="row  rounded round center" style="width:100%;max-width:100%;">
<div  class="border1  center rounded round" style="width:100%;max-width:100%;"><center><h1>MEMBERS:</h1></center>';

echo '       <table class="table table-striped table-hover">
          <thead>
            <tr>
              <th><center>Icon|Picture</center></th>
              <th><center>Title|Salutation<br>Status</center></th>
              <th><center>Name<br>(FirstName|LastName)<br>Email</center></th>
              <th><center>Principal|OID<br>(click to manage Mailboxes&Subscriptions)</center></th>
            </tr>
          </thead>
          <tbody>';

$syslog=$syslog.'<script>console.log(\' TIMING_HEAD:'.( microtime(true) - $start_time).' s \');</script>';

// list members
$imagekeys=json_decode(graph_getAllImageCacheKeys(),true);

$memberlog="";
$memberimagejavascript="";
$membincrcount=0;
$firstimagedelay=423;//ms so we the browser catches 304 with etag
$imagedelay=42;//ms so we the browser catches 304 with etag
$startdelay=$firstimagedelay;
          foreach ($members as $member) {

            $persIMG=base_url()."/assets/person.svg";
            $membincrcount=$membincrcount+1;
            $startdelay=$startdelay+$imagedelay;
            //is it a guest ..
            if(graph_PrincipalIsExternal($member["userPrincipalName"])) {
                     $membertype="Guest";
                     // is the guest confirmed ...
                      if(isset($guests[$member["id"]]["externalUserState"])) {
                        if($guests[$member["id"]]["externalUserState"] ==  "PendingAcceptance" ) {
                          $persIMG=base_url()."/assets/user-unconfirmed.webp";
                        }
                        $membertype=$membertype."<br>(".str_replace("PendingAcceptance", "<b>Unconfirmed</b>",$guests[$member["id"]]["externalUserState"]).")";
                      }
            } else { $membertype="<b>Member</b>"; }
          //$imagekey=graph_getImageCacheKeyByOID($member["id"]);
          $imagekey="";
          if(isset($imagekeys[ md5($member["id"]) ] ) ) {
              $imagekey=$imagekeys[md5($member["id"])];
				   }
          if(is_string($imagekey) && !empty($imagekey) && strlen($imagekey) > 16 ) {
            // we found an assigned image


          //$res=graph_getImageByOID($member["id"]) ;
          //          if($res["status"] == "OK") {
          //var_dump($res);

              //return unserialize($redis->get($cache_key));
              //return md5(base64_decode($res["result"]));
              //$member["imagekey"]=md5(base64_decode($res["result"]));
              //$img=base64_decode($res["result"]);
              //inline
              //$persIMG='data:image/jpeg;base64,'.$res["result"];

              //var img = document.getElementById("icon-oid-"+oid);
              //img.src = "data:image/jpeg;base64," + doc.image;


            //$memberimagejavascript=$memberimagejavascript.'var doc = {  "imagekey": "'.$imagekey.'","oid": "'.$member["id"].'"  };imgArray.push(doc);'." \n ";
            //save some requests and throw inline images from script as it knows how to 304 etag
            //$persIMG=base_url().'/SelfAdmin.php?checkSUM='.$imagekey.'&GETSelfAction=getImageContentByCheckSum';

            //save some requests and lazy load images from script as it knows how to 304 etag
            $lateIMG=base_url().'/SelfAdmin.php?checkSUM='.$imagekey.'&GETSelfAction=getImageContentByCheckSum';
            //$lateIMG=base_url().'/SelfAdmin.php?oid='.$member["id"].'&GETSelfAction=getImageContentByOID';

            $memberimagejavascript=$memberimagejavascript.'setTimeout( function(){ document.getElementById("icon-oid-'.$member["id"].'").src="'.$lateIMG.'"; } , '.$startdelay.' );';

          } else {

              $memberimagejavascript=$memberimagejavascript.'var doc = {  "action": "getImageSumByOID","_id": "'.$member["id"].'"  };usrArray.push(doc);'." \n ";
          }

			   $memberlog="\n ".$memberlog.'<script>console.log(\' MEMBER:'.json_encode($member).'\');</script>';


         if($membincrcount=1) { $startdelay=2; }
//         $memberimagejavascript=$memberimagejavascript." \n ".'<script>
//         setTimeout(function(){
//            document.getElementById("icon-oid-'.$member["id"].'").src="/SelfAdmin.php?GETSelfAction=getImageContentByOID&oid='.$member["id"].'"; }, '.$startdelay.');
//         </script>';
///////////         $memberimagejavascript=$memberimagejavascript." \n ".'<script>
///////////         setTimeout(function(){
///////////         var doc = {
///////////                   "name": "'.$member["displayName"].'",
///////////                   "jobTitle": "'.$member["jobTitle"].'",
///////////                   "action": getImageContentByOID,
///////////                //   "hobbies": [
///////////                //     "playing with balls of yarn",
///////////                //     "chasing laser pointers",
///////////                //     "lookin hella cute"
///////////                //   ]
///////////                   "_id": "'.$member["id"].'"
///////////                 };
///////////        usrdb.put(doc);
///////////        dbinfo_usr(); }, '.$startdelay.');
///////////         </script>';

//echo              "name": "\'.$member["displayName"].\'",
//                   "jobTitle": "\'.$member["jobTitle"].\'", \';



        //colorize the icons for the preload
        $iconstyle="width:64px;height:64px;";
        if(graph_PrincipalIsExternal($member["userPrincipalName"])) {
           $iconstyle=$iconstyle.'background-color: lightblue;';
        }
			  echo '
             <tr>
                <td><div id="icon-container-oid-'.$member["id"].'" style="width:68px;height:68px;" class="round rounded" ><center><img  loading="lazy" id="icon-oid-'.$member["id"].'"  style="'.$iconstyle.'" src="'.$persIMG.'"></center></div></td>
                <td><center><code>'.$member["jobTitle"].'</code></center><br>';
        echo   '<div class="membertype round rounded brightbg" style="border: 1px solid #000;" ><center>'.$membertype.'</center></div></td>
                <td><code>'.$member["displayName"].'<br> (  '.$member["givenName"].' | '.$member["surname"].'  ) </code><br><a href="mailto:'.$member["mail"].'" >'.$member["mail"].'</a></td>
                <td><a href="'.graph_LicenseUserSettingsURLFromOID($member["id"]).'"><code>'.$member["userPrincipalName"].'</code><br><code><b>'.$member["id"].'</b></code></a></td>
              </tr>';
		  }
//end table and file

echo '
          </tbody>
        </table>';
$syslog=$syslog.'<script>console.log(\' TIMING_MEMBERLIST:'.( microtime(true) - $start_time).' s \');</script>';

print "\n ".'  <script>
  var usrdb = new PouchDB(\'usr\');
  function dbinfo_usr() {
    usrdb.info().then(function (info) {
                        console.log(info);    })
  }
  var cachedb = new PouchDB(\'cache\');
  function dbinfo_cache() {
    cachedb.info().then(function (info) {
                        console.log(info);    })
  }
  var imgdb = new PouchDB(\'img\');

  function dbinfo_img() {
    imgdb.info().then(function (info) {
                         console.log(info);  })
  }
  //empty array to use bulk_docs and save some requests
  usrArray = [];
  imgArray = [];
   </script> ';

print " \n <script>".$memberimagejavascript."</script> \n ";
$syslog=$syslog.'<script>console.log(\' TIMING_JAVASCRIPTHEAD:'.( microtime(true) - $start_time).' s \');</script>';

echo '

<script>
	function HTTPRequest(params){

	if(!params || !params.url){
		alert(\'No params passed!!\');
		return;
	}

	if(!params.success){
		alert(\'Give the XHR call a success callback!!\');
		return;
	}
	if(!params.error){
		alert(\'Give the XHR call an error callback!!\');
		return false;
	}
	if(!params.type){ params.type = \'GET\'; }


function transferComplete () {
  //console.log(this.responseText);
  params.success(this.responseText);
}
function transferCanceled () {
  //console.log(this.responseText);
  params.error(this.responseText);
}
function transferFailed () {
  //console.log(this.responseText);
  params.error(this.responseText, e);
}
	var oReq = new XMLHttpRequest();

//        oReq.addEventListener("progress", updateProgress);
//        oReq.addEventListener("abort", transferCanceled);
//
//        oReq.addEventListener("error", transferFailed);
//        oReq.addEventListener("load", transferComplete);
//        oReq.addEventListener("load", reqListener);

        oReq.addEventListener("abort", transferCanceled);
        oReq.addEventListener("error", transferFailed);
        oReq.addEventListener("load", transferComplete);
        if(!params.update){
        //danger,will slow down
         //console.log("no partial update function for XHR");
        } else {
        function updateProgress () {
           //console.log("update" + this.responseText);
           params.update(this.responseText);
           }
        oReq.addEventListener("progress", updateProgress);
        }
        oReq.open("GET", params.url);
        oReq.send();
   }

    </script>

<script>
//   usrdb.bulkDocs(usrArray);
//   usrArray = [];
//   dbinfo_usr();
//   usrdb.allDocs({ include_docs: true,  attachments: true     }).then(
//        function (result) {
//           // handle result
//           //console.log(result);
//           console.log("usrdb.allDocs");
//           usrArray=result["rows"];
//           }
//         ).catch(function (err) {
//           console.log(err);
//        });
//        console.log(usrArray);

// Create Base64 Object
var Base64={_keyStr:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",encode:function(e){var t="";var n,r,i,s,o,u,a;var f=0;e=Base64._utf8_encode(e);while(f<e.length){n=e.charCodeAt(f++);r=e.charCodeAt(f++);i=e.charCodeAt(f++);s=n>>2;o=(n&3)<<4|r>>4;u=(r&15)<<2|i>>6;a=i&63;if(isNaN(r)){u=a=64}else if(isNaN(i)){a=64}t=t+this._keyStr.charAt(s)+this._keyStr.charAt(o)+this._keyStr.charAt(u)+this._keyStr.charAt(a)}return t},decode:function(e){var t="";var n,r,i;var s,o,u,a;var f=0;e=e.replace(/[^A-Za-z0-9\+\/\=]/g,"");while(f<e.length){s=this._keyStr.indexOf(e.charAt(f++));o=this._keyStr.indexOf(e.charAt(f++));u=this._keyStr.indexOf(e.charAt(f++));a=this._keyStr.indexOf(e.charAt(f++));n=s<<2|o>>4;r=(o&15)<<4|u>>2;i=(u&3)<<6|a;t=t+String.fromCharCode(n);if(u!=64){t=t+String.fromCharCode(r)}if(a!=64){t=t+String.fromCharCode(i)}}t=Base64._utf8_decode(t);return t},_utf8_encode:function(e){e=e.replace(/\r\n/g,"\n");var t="";for(var n=0;n<e.length;n++){var r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r)}else if(r>127&&r<2048){t+=String.fromCharCode(r>>6|192);t+=String.fromCharCode(r&63|128)}else{t+=String.fromCharCode(r>>12|224);t+=String.fromCharCode(r>>6&63|128);t+=String.fromCharCode(r&63|128)}}return t},_utf8_decode:function(e){var t="";var n=0;var r=c1=c2=0;while(n<e.length){r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r);n++}else if(r>191&&r<224){c2=e.charCodeAt(n+1);t+=String.fromCharCode((r&31)<<6|c2&63);n+=2}else{c2=e.charCodeAt(n+1);c3=e.charCodeAt(n+2);t+=String.fromCharCode((r&15)<<12|(c2&63)<<6|c3&63);n+=3}}return t}}

var downloading=[];
var downloadDone=[];
var imagecache=[];
var doneimages=[];
function getAndFillImage(oid,sum,origDocument) {
  //get the image if we don not have it yet
  //imgdb.get(sum).then(function (doc) {
    if(downloadDone[sum]==true && imagecache[sum] !== undefined) {


    // handle doc that we already have
    // we have to find the oid named icons and set them the right img src
    //document.getElementById("icon-oid-'.$member["id"].'").src=
    console.log("document.getElementById("+ "icon-oid-"+oid  );
    //console.log(doc);
    var img = document.getElementById("icon-oid-"+oid);
    //img.src = "data:image/jpeg;base64," + doc.image;
    img.src = "data:image/jpeg;base64," + imagecache[sum];
    console.log("deleting "+oid+" "+sum);
    console.log(cachedb.remove(origDocument));
    doneimages[oid]=false;
  //}).catch(function (err) {
    }
    if(downloading[sum]==false && downloadDone[sum]==false && imagecache[sum] === undefined) {

      downloading[sum]=true;
      // we need to fetch the doc
      var paramURL="'.base_url().'/SelfAdmin.php?checkSUM="+sum+"&GETSelfAction=getImageContentByCheckSum&base64=true";
      console.log("DwnLoad FROM :"+paramURL);
      HTTPRequest({
      url: paramURL ,
      type:\'GET\',
      success: function(a){
        console.log("img content result for ::: " + sum + " ::: ");
        //console.log(  a);
        //var json = JSON.parse(a);
        var now = new Date().getTime();
        var elapsed = now - proc_uptime;
        ///var doc = {
        ///          "ImageSUM": sum,
        ///          "image": a,
        ///          "_id": sum,
        ///          "time": now,
        ///          "uptime": millisToMinutesAndSeconds(elapsed)
        ///        };
        ///imgdb.put(doc);
        imagecache[sum]=a
        downloading[sum]=false;
        downloadDone[sum]=true;
      //  document.getElementById("tty").innerHTML =  \'<center>sync DONE </center><br />\' + a;
      //        var myDiv = document.getElementById("tty");
      //        myDiv.scrollTop = myDiv.scrollHeight;


        // do something else
      },
     // update: function(b){
     //   //var json = JSON.parse(b);
     //   console.log("update : " + b);
     // //	filltty(\'<center>sync DONE </center><br />\' + b);
     //   // do something else
     // },
      error: function(response, message){
             console.log("XHR failed");
             console.log("response");
             console.log(response);
             console.log("error");
             console.log(message);
             downloading[sum]=false;

        // handle error
      }
    }); //end httpreq
    }

  //}); // end imgdb.sum



}


function fillOIDiconFromCache() {
console.log("cron: ");dbinfo_cache();
////////
  cachedb.allDocs({

    include_docs: true,
    attachments: true
  }).then(function (result) {
        // handle result
    //console.log(result["rows"]);
    rows=result["rows"];
    console.log(rows);
    for(i=0;i<rows.length && rows.length > 0;i++) {
             console.log(rows[i].doc.ImageSUM.length);
              //   console.log("sched: n:" + i + "( ID : " + rows[i].id + " )" + " raw:" );
             if(rows[i].doc.ImageSUM.length >16) {
              if(doneimages[rows[i].id] !== undefined && doneimages[rows[i].id]==false ) {

                console.log("queuing fetch for: ID : " + rows[i].id + " SUM: "  + rows[i].doc.ImageSUM);
                //setTimeout(function() { getAndFillImage(rows[i].id,rows[i].doc.ImageSUM) } ,20 );
                getAndFillImage(rows[i].id,rows[i].doc.ImageSUM,rows[i].doc);
                }
              }



    }
    // handle result end
    }).catch(function (err) {
    console.log(err);
  });
/////////

setTimeout(function() { fillOIDiconFromCache(); } , 2222 );
} //end fillOIDiconFromCache

//var cronIntervalID=setInterval(fillOIDiconFromCache,22350);
//setTimeout(function() { fillOIDiconFromCache(); },666 );
fillOIDiconFromCache();

function resolveImage(paramURL,id) {
  HTTPRequest({
  url: paramURL ,
  type:\'GET\',
  success: function(a){

    console.log("img cachekey result for ::: " + paramURL + " ::: " + a);

    var json = JSON.parse(a);
    var elapsed = new Date().getTime() - proc_uptime;
    //console.log(json);
    if(json["keysum"]!="") {
      // do nothing if the key exists
      cachedb.get(json["oid"]).then(function (doc) {
        // handle doc //do nothing if the key exists
      }).catch(function (err) {
        var doc = {
                  "ImageSUM": json["keysum"],
                  "action": "getImageSumByOID",
                  "_id": json["oid"],
                  "uptime": millisToMinutesAndSeconds(elapsed)
                };
        cachedb.put(doc);
      });
    }

  //  document.getElementById("tty").innerHTML =  \'<center>sync DONE </center><br />\' + a;
  //        var myDiv = document.getElementById("tty");
  //        myDiv.scrollTop = myDiv.scrollHeight;


    // do something else
  },
 // update: function(b){
 //   //var json = JSON.parse(b);
 //   console.log("update : " + b);
 // //	filltty(\'<center>sync DONE </center><br />\' + b);
 //   // do something else
 // },
  error: function(response, message){
         console.log("XHR failed");
         console.log("response");
         console.log(response);
         console.log("error");
         console.log(message);


    // handle error
  }
  });
}
   for(i=0;i<usrArray.length;i++) {
     //console.log("action : " + usrArray[i]["doc"]["action"] + " " + usrArray[i]["_id"]);
     console.log("action "+i+": " + usrArray[i]["action"] + " " + usrArray[i]["_id"]);
     var ThisURL="'.base_url().'/SelfAdmin.php?oid="+usrArray[i]["_id"]+"&GETSelfAction="+usrArray[i]["action"];
     var ThisID=usrArray[i]["_id"];
     //cachedb
    resolveImage(ThisURL,ThisID);
    //setTimeout(function() { resolveImage("'.base_url().'/SelfAdmin.php?oid="+usrArray[i]["_id"]+"&GETSelfAction="+usrArray[i]["action"] , usrArray[i]["_id"]); } , i*20 ) ;
    }
  usrArray=[];
  dbinfo_usr();
  usrdb.allDocs({ include_docs: true,  attachments: true     }).then(
       function (result) {
          // handle result
          //console.log(result);
          console.log("cachedb.allDocs TO usrArray");
          //usrArray=result["rows"];
          for(m=0;m<result["rows"].length;m++) {
              console.log(result["rows"][m]);
              //get the real image
              }
          }
        ).catch(function (err) {
          console.log(err);
       });
  //console.log(usrArray);

   </script> ';
  if (Settings::$debugAppVerbose) {
print "\n".$memberlog."\n";
}

echo   '<hr>
        <br>
      </div>
      <hr>
      <br>
    </div>
  </div>';

echo '<hr> <div class="round border1 brightbg" id="renderTime" >  <center>Render time: '.( microtime(true) - $start_time).' seconds   </center></div>';

echo ' </div>' ;
print($syslog);
echo' </body>
</html>' ;

?>
