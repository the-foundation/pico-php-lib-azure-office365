<?php
//error_reporting(E_ALL|E_STRICT);
//ini_set("display_errors", 1);



if (!class_exists("Settings"))	 {

require_once("Settings.php");
}
require_once("AuthHelper.php");
require_once("pico_lib_functions.php");

require_once "MS_graph_mailer.php";
function graph_PseudoGuestMailPrincipalUserNameFromMailAddress($mail) {
    return "guest_usr_".md5($mail);
}

function graph_PseudoGuestMailListPrincipalUserNameFromMailAddress($mail) {
    return "guest_mli_".md5($mail);
}

function graph_DeleteGuestUsersWithNoGroupMembership() {
  $results=array();
  foreach(graph_getGuestUsersWithNoGroupMembership() as $deleteuser)
  {
    array_push($results,graph_DeleteUser($deleteuser));
  }
  return $results;
}

function graph_DeleteUser($oid) {
        $token = AuthHelper::getDaemonToken();
        $targetURL = Settings::$unifiedAPIEndpoint . "users/" . $oid;
        $request = curl_init($targetURL);
        curl_setopt($request, CURLOPT_HTTPHEADER, [
           "Authorization: Bearer " . $token->accessToken,
           "Accept: application/json",
           "Content-Type:application/json",
         ]);
        //FAILS: curl_setopt($request	, CURLOPT_PATCH, true);
        //curl_setopt($request, CURLOPT_CUSTOMREQUEST, 'PATCH');
        curl_setopt($request, CURLOPT_CUSTOMREQUEST, 'DELETE');
        if ( Settings::$debugAppVerbose   ) {
		     	print "\nsend DELETE TO ".$targetURL;
        }

        //  curl_setopt($request, CURLOPT_POSTFIELDS, $messageJSON);
        curl_setopt($request, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($request);
        //echo '<script>console.log(\' RES: '.$response.'\');</script>';
        //parse the json into oci_fetch_object

        //return $result;
        //var_dump($response);
        //return json_decode($response, true);

        ///var_dump(json_decode($response));


        if (curl_getinfo($request, CURLINFO_HTTP_CODE) == "202") {
            // oh wow , something at MS works..
            return array ( "status"=>"OK","result"=> json_decode($response, true) );
        } else {
			        $result = json_decode($response, true);
              if(isset ($result["error"])  && isset ($result["error"]["message"]) && strpos($result["error"]["message"],'One or more added object references already exist for the following modified properties') !== false)  {
 			                      //also ok because we want to add them anyway
                            return array ( "status"=>"OK","result"=>$result );
 			       } else {
             return array ( "status"=>"FAIL","result"=>$result );

           }
        }
  }


//ATT <<when adding an user to a group, you can add up to 20 users per request, into a group. add member
//ATT https://stackoverflow.com/questions/64660341/is-there-any-posibility-to-delete-multiple-members-of-a-group-with-microsoft-gra
function graph_AddUsersToGroup($groupArgs) {

      //  if (Settings::$debugAppVerbose   || Settings::$debugApp|| Settings::$debugAppMail ) { $dumpArgs=true;  }

        if ( Settings::$debugAppVerbose  || Settings::$debugApp   ) {
            print(" graph_AddUsersToGroup");
            print(json_encode($groupArgs,JSON_PRETTY_PRINT) );
        }
        /*
        {
$jsn='{
    "members@odata.bind": [
        "https://graph.microsoft.com/v1.0/directoryObjects/abc00111-1111-22bb-33cc-666123abc999"
    ]
}';
        $groupArgs[
                AddToGroupOID,
                userOIDlist[]
                ]
        */


        if(!isset($groupArgs["AddToGroupOID"])) { return "false" ;}
        if(!isset($groupArgs["userOIDlist"]))   { return "false" ;}
        if(empty($groupArgs["userOIDlist"]))   { return "false" ;}

        $addusers=array();
        //catch false param type
       if (is_string($groupArgs["userOIDlist"])) {
         if ( Settings::$debugAppVerbose  || Settings::$debugApp    ) {
             print(" softfail: got single string" );
         }
		   $groupArgs["userOIDlist"]=array($groupArgs["userOIDlist"]);
	   }

/////////////////       foreach ($groupArgs["userOIDlist"] as $userOID) {
/////////////////         //echo "add $userOID";
/////////////////         if (!empty($userOID))
/////////////////         {
/////////////////         array_push($addusers,"https://graph.microsoft.com/v1.0/directoryObjects/".$userOID);
/////////////////         }
/////////////////	    }

     if ( Settings::$debugAppVerbose  || Settings::$debugApp ) {
          print(" ADDING USERS: " );
          print(json_encode($groupArgs,JSON_PRETTY_PRINT) );
      }

  //due to chunkSize of max 20, we take 10 to be safe
        $chunk_count=0;
        $maxChunkSize=10;
        if( isset( Settings::$GraphMaxAddUsersPerRequest)) {
		    	$maxChunkSize = Settings::$GraphMaxAddUsersPerRequest;
		    }
        $token = AuthHelper::getDaemonToken();
        $targetURL = Settings::$unifiedAPIEndpoint . "groups/" . $groupArgs["AddToGroupOID"];

        //	 var_dump($targetURL);


	    	$subresults=array();
	    	$returnObject=array( "status"=>"NOT_STARTED","result"=> array());
	    	$allfailed=true;   // will be unset by any successfull request
	    	$nonefailed=true;  //check for partial failes since we run chunked
	    	$returnObject["chunks_failed"]=array(); // the array chunk index +1
	    	$returnObject["chunks_ok"]=array(); // the array chunk index +1
        $existingGroupMembers=array();
        foreach ( graph_getGroupByOID($groupArgs["AddToGroupOID"])["value"]  as $existing ) {
                    array_push($existingGroupMembers,$existing["id"]);
                   }
        //foreach ( array_chunk($addusers, $maxChunkSize , true) AS $adduserbuffer  ) {

        $tmpOIDlist=$groupArgs["userOIDlist"];
        $groupArgs["userOIDlist"]=array();

        foreach (  array_unique($tmpOIDlist) as $testme ) {
          if ( !in_array($testme , $existingGroupMembers)  ) {
           array_push($groupArgs["userOIDlist"],$testme);
          }
        }

        if ( Settings::$debugAppVerbose  || Settings::$debugApp ) {
            print(" RAW REQUESTED :".count($tmpOIDlist)." IDs .. without existing ones".count($groupArgs["userOIDlist"])." having".count($existingGroupMembers)." existing members before \n" );
            print(json_encode($groupArgs,JSON_PRETTY_PRINT) );
        }
        for ($index = 0; $index <= count($groupArgs["userOIDlist"]); $index=$index+$maxChunkSize ) {
          $lastindex=$index+$maxChunkSize;
          if(count($groupArgs["userOIDlist"]) < ($lastindex+1) )
           {
             $lastindex=count($groupArgs["userOIDlist"])-1;
           }
           $adduserbuffer=array();
          for ($addme=$index;$addme<=$lastindex;$addme++) {
            array_push($adduserbuffer,"https://graph.microsoft.com/v1.0/directoryObjects/".$groupArgs["userOIDlist"][$addme]);
          }

			    if(		$returnObject["status"] == "NOT_STARTED" ) {
			          $returnObject["status"] == "STARTED";
		            }

        	$chunk_count=$chunk_count+1;
          echo "+";
          $messageJSON='{
             "members@odata.bind": '.json_encode($adduserbuffer).'
             }';
             if ( Settings::$debugAppVerbose   ) {
                 print_r($messageJSON)   ;
   	             print  "\nSENDING CHUNK ".$chunk_count."  TO ".$targetURL." \n";
                 var_dump($adduserbuffer);
                 print  "\nCHUNK JSON ".$chunk_count."  TO ".$targetURL." \n";
                 print_r(htmlentities($messageJSON, ENT_QUOTES));
		       }
        	//print_r($chunk_count);//print_r($currentList);
             $request = curl_init($targetURL);
             curl_setopt($request, CURLOPT_HTTPHEADER, [
                "Authorization: Bearer " . $token->accessToken,
                "Accept: application/json",
                "Content-Type:application/json",
                //  "Content-Type: text/plain"
            ]);

        	       //curl_setopt($request, CURLOPT_POST, true);

        //FAILS: curl_setopt($request	, CURLOPT_PATCH, true);
        curl_setopt($request, CURLOPT_CUSTOMREQUEST, 'PATCH');

        if ( Settings::$debugAppVerbose   ) {
			print "\nsend TO ".$targetURL;
            print_r(htmlentities($messageJSON, ENT_QUOTES));

        }

        curl_setopt($request, CURLOPT_POSTFIELDS, $messageJSON);
        curl_setopt($request, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($request);
        //echo '<script>console.log(\' RES: '.$response.'\');</script>';
        //parse the json into oci_fetch_object

        //return $result;
        //var_dump($response);
        //return json_decode($response, true);

        ///var_dump(json_decode($response));


        if (curl_getinfo($request, CURLINFO_HTTP_CODE) == "202") {
		    //$result = json_decode($response, true);
            //return array ( "status"=>"OK","result"=>$subresults );
            $allfailed=false;
            array_push($subresults,$response);
            array_push($returnObject["chunks_ok"],$chunk_count);

        } else {
			     $result = json_decode($response, true);
			    if(isset ($result["error"])  && isset ($result["error"]["message"]) && strpos($result["error"]["message"],'One or more added object references already exist for the following modified properties') !== false)  {
          //fail
			     $allfailed=false;
			    return array ( "status"=>"FAIL","reason"=>"SOME_OR_ALL_USERS_ALREADY_IN_GROUP","result"=>$subresults);
			    //array_push($subresults,$response);
          //array_push($returnObject["chunks_ok"],$chunk_count);
			 } else {
                 $nonefailed=false;
				 //failed
				 //return array ( "status"=>"FAIL","result"=>$subresults);
			     array_push($subresults,$response);
                 array_push($returnObject["chunks_failed"],$chunk_count);
				 } // end else errormessage says request is okay (softfail) but users were in group

			}  // end else response not 202

    } //end foreach chunked

            if($nonefailed==true) {
				$returnObject["status"]="OK";
				} else {
					$returnObject["status"]="FAIL";
					}

            $returnObject["result"]=$subresults;

            return 	$returnObject;


    }



// end add user to group



///sample error://    "error": {
///sample error://        "code": "Request_BadRequest",
///sample error://        "message": "One or more added object references already exist for the following modified properties: 'members'.",
///sample error://        "innerError": {
///sample error://            "date": "2022-03-09T05:41:47",
///sample error://            "request-id": "9a4f35f0-a5e6-46e7-be4e-c784e4cec1c7",
///sample error://            "client-request-id": "e5b1702d-7946-6827-5cd8-c3f8c1d696dd"
///sample error://        }
///sample error://    }

function graph_DeleteUsersFromGroup($groupArgs,$queueCommands=false) {
/*
$groupArgs[
        DelFromGroupOID,
        userOID
        ]
 $jsn='{
     "members@odata.bind": [
         "https://graph.microsoft.com/v1.0/directoryObjects/abc00111-1111-22bb-33cc-666123abc999"
     ]
 }';
*/
        $dumpArgs=false;
      //  if (Settings::$debugAppVerbose   || Settings::$debugApp|| Settings::$debugAppMail ) { $dumpArgs=true;  }
        if (Settings::$debugAppVerbose   ) { $dumpArgs=true;  }
        if($dumpArgs) {
            print("GOT USERdelFROMgroup request \n");
            print(json_encode($groupArgs,JSON_PRETTY_PRINT) );
        }


        $delusers=array();
        if(!isset($groupArgs["DelFromGroupOID"])) { return "false" ;}
        if(!isset($groupArgs["userOIDlist"])) { return "false" ;}
      foreach ($groupArgs["userOIDlist"] as $userOID) {
         //echo "add $userOID";
         if (!empty($userOID))
         {
           if($dumpArgs) {
               print("ADDING TO DEL-LIST  \n");
               print(json_encode($userOID,JSON_PRETTY_PRINT) );
           }
           array_push($delusers,$userOID);
         }
	    }
	    $retstate=true;//would return ok by default
	    $subresults=array();//results by oid
         foreach ( array_unique($delusers) as $deluser) {
			     $subresults[$deluser] = graph_DeleteUserFromGroup(array(
			                                          "DelFromGroupOID" => $groupArgs["DelFromGroupOID"],
			                                          "userOID" => $deluser,
			                                           )
			                                      );

		 }
		 if($retstate) { $retcode="OK"; } else { $retcode="FAIL" ; }
		 return array(
		       "status" => $retcode ,
		       "results" => $subresults
		       );
}

function graph_DeleteUserFromGroup($groupArgs) {
        $dumpArgs=false;
      //  if (Settings::$debugAppVerbose   || Settings::$debugApp|| Settings::$debugAppMail ) { $dumpArgs=true;  }
        if (Settings::$debugAppVerbose   ) { $dumpArgs=true;  }
        if($dumpArgs) {
            print(json_encode($groupArgs,JSON_PRETTY_PRINT) );
        }
        /*

         $jsn='{
             "members@odata.bind": [
                 "https://graph.microsoft.com/v1.0/directoryObjects/abc00111-1111-22bb-33cc-666123abc999"
             ]
         }';
        $groupArgs[
                DelFromGroupOID,
                userOID
                ]
        */
        if(!isset($groupArgs["DelFromGroupOID"])) { return "false" ; }
        if(!isset($groupArgs["userOID"]))         { return "false" ; }
        if(is_array($groupArgs["userOID"]))         {
          if($dumpArgs) {
             print_r(htmlentities(json_encode($groupArgs["userOID"],JSON_PRETTY_PRINT), ENT_QUOTES));
           }
          return array ( "status"=>"FAIL","reason"=>"OID NOT A SINGLE STRING");
        }
       /// //catch false param type
       ///if (is_string($groupArgs["userOIDlist"])) {
		///   $groupArgs["userOID"]=array($groupArgs["userOIDlist"]);
	   ///}
       ///foreach ($groupArgs["userOID"] as $userOID) {
       ///    //echo "add $userOID";
       ///    if (!empty($userOID))
       ///    {
       ///        array_push($addusers,"https://graph.microsoft.com/v1.0/directoryObjects/".$userOID);
       ///    }
       ///}

       $messageJSON='{
                     "members@odata.bind": '.json_encode(array("https://graph.microsoft.com/v1.0/directoryObjects/".$groupArgs["userOID"])).'
                     }';

       if($dumpArgs) {
                    var_dump(json_decode($messageJSON))   ;
        }
        $token = AuthHelper::getDaemonToken();
        $targetURL =
            Settings::$unifiedAPIEndpoint.'groups/'.$groupArgs["DelFromGroupOID"]."/members/".$groupArgs["userOID"].'/$ref';
        //	 var_dump($targetURL);
        $request = curl_init($targetURL);
        curl_setopt($request, CURLOPT_HTTPHEADER, [
            "Authorization: Bearer " . $token->accessToken,
            "Accept: application/json",
            "Content-Type:application/json",
            //  "Content-Type: text/plain"
        ]);

       //curl_setopt($request, CURLOPT_POST, true);

        //FAILS: curl_setopt($request	, CURLOPT_PATCH, true);
        curl_setopt($request, CURLOPT_CUSTOMREQUEST, 'DELETE');

        if($dumpArgs) {
			    echo "send DELfromGROUP TO ".$targetURL;
            var_dump($messageJSON);
        }

        curl_setopt($request, CURLOPT_POSTFIELDS, $messageJSON);
        curl_setopt($request, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($request);
        //echo '<script>console.log(\' RES: '.$response.'\');</script>';
        //parse the json into oci_fetch_object
        //$result = json_decode($response, true);
        //return $result;
        //var_dump($response);
        //return json_decode($response, true);
        ///var_dump(json_decode($response));

        if (curl_getinfo($request, CURLINFO_HTTP_CODE) == "204") {
            //graph API OK direct
            return array ( "status"=>"OK","result"=>json_decode($response, true));
        } else {
            // a non-group-member cannot be removed again
            // return OK in that case as well
            //parse the json into oci_fetch_object
            $result = json_decode($response, true);
			if(isset ($response["error"])  && isset ($response["error"]["message"]) && strpos($response["error"]["message"],'does not exist or one of its queried reference-property objects are not present') != false)  {
             return array ( "status"=>"OK","result"=>$result);
			     } else {
				      //failed for real
				      return array ( "status"=>"FAIL","http_status_code"=>curl_getinfo($request, CURLINFO_HTTP_CODE),"result"=>$response);
				    }  // end else errormessage says request is okay (softfail) but users were not in group so made 1  bogus request
			} // end else response not 204
    } // end delete user from group


    ////PATCH https://graph.microsoft.com/v1.0/users/{id}
    ////Content-type: application/json
    ////
    ////{
    ////  "businessPhones": [
    ////    "+1 425 555 0109"
    ////  ],
    ////  "officeLocation": "18/2111"
    //      "jobTitle": "Mister Mister",
    //      "surname": "Lastname",
    //      "givenName": "Firstname"
    ////}

function graph_UpdateuserNameFields($oid,$argAray) {
if(!isset( $oid)) {
    return false;
  }

$dumpArgs=false;
//  if (Settings::$debugAppVerbose   || Settings::$debugApp|| Settings::$debugAppMail ) { $dumpArgs=true;  }
if (Settings::$debugAppVerbose   ) { $dumpArgs=true;  }

$str='{ ';
foreach (array("jobTitle","surname","givenName") as $index) {
  if(isset($argAray[$index]) && !empty($argAray[$index]) ) {
  $str=$str.'"'.$index.'": "'.$argAray[$index].'",';
  }
}
$str=$str.'}';
$token = AuthHelper::getDaemonToken();
$messageJSON=$str;
$targetURL =
    Settings::$unifiedAPIEndpoint.'users/'.$oid;
//	 var_dump($targetURL);
$request = curl_init($targetURL);
curl_setopt($request, CURLOPT_HTTPHEADER, [
    "Authorization: Bearer " . $token->accessToken,
    "Accept: application/json",
    "Content-Type:application/json",
    //  "Content-Type: text/plain"
]);

//curl_setopt($request, CURLOPT_POST, true);

//FAILS: curl_setopt($request	, CURLOPT_PATCH, true);
curl_setopt($request, CURLOPT_CUSTOMREQUEST, 'PATCH');

if($dumpArgs) {
  echo "send DELfromGROUP TO ".$targetURL;
    var_dump($messageJSON);
}

curl_setopt($request, CURLOPT_POSTFIELDS, $messageJSON);
curl_setopt($request, CURLOPT_RETURNTRANSFER, true);
$response = curl_exec($request);
return $response;
}

function graph_getImageCacheKeyByOID($oid)
{
    if(!isset($redis)) { $redis = new Redis(); $redis->connect(Settings::$redis_host, Settings::$redis_port); }

    $cache_key = "ms_graph_api_user_image_".md5($oid);
    //get the real cache key from the user entry
    if ($redis->exists($cache_key)) {
      return unserialize($redis->get($cache_key));
    } else {
      $res=graph_getImageByOID($oid) ;
      //var_dump($res);
      if($res["status"] == "OK") {
          //return unserialize($redis->get($cache_key));
          return md5(base64_decode($res["result"]));
      }
     }
}

function graph_getImageByCheckSum($cacheSum , $useCache = true ) {
        if(!isset($redis)) { $redis = new Redis();  $redis->connect(Settings::$redis_host, Settings::$redis_port); }
        $cache_key="ms_graph_api_image_".$cacheSum;
        // $data_source = "Data from Redis Server";
        if ($redis->exists($cache_key) && $useCache == true) {
           return array ( "status"=>"OK","source"=>"CACHE","result"=> base64_encode(unserialize($redis->get($cache_key))) );
        } else { return array ( "status"=>"FAIL", "reason" => "CACHE_NOT_FOUND" ); }
}

function graph_getImageByOID($oid,$useCache = true)
{
    if(!isset($redis)) { $redis = new Redis(); }
    $redis->connect(Settings::$redis_host, Settings::$redis_port);
    $cache_key = "ms_graph_api_user_image_".md5($oid);
    $doUpdate=false;

    //get the real cache key from the user entry
    if ($redis->exists($cache_key) && $useCache == true) {
        $cache_key="ms_graph_api_image_".$redis->get($cache_key);
        // $data_source = "Data from Redis Server";
        if ($redis->exists($cache_key) && $useCache == true) {
           return array ( "status"=>"OK","source"=>"CACHE","result"=> base64_encode(unserialize($redis->get($cache_key))), true );
        } else { $doUpdate=true; }
    } else { $doUpdate=true; }

    if($useCache == false) {
      $doUpdate=true;
    }
    if($doUpdate) {
        $token = AuthHelper::getDaemonToken();
        $targetURL = Settings::$unifiedAPIEndpoint . 'users/'.$oid.'/photo/$value';
        $request = curl_init($targetURL);
        curl_setopt($request, CURLOPT_HTTPHEADER, [
            "Authorization: Bearer " . $token->accessToken,
            "Accept: application/json",
        ]);
        curl_setopt($request, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($request);
        //var_dump(base64_encode($response));
        //print_r($targetURL);
        //echo '<script>console.log(\' IMG RES: '.$response.'\');</script>';
        //parse the json into oci_fetch_object
        //var_dump($response);
         if (curl_getinfo($request, CURLINFO_HTTP_CODE) == "200") {
            // oh wow , something at MS works..
            $responseChecksum=md5($response);
            $img_cache_key="ms_graph_api_image_".$responseChecksum;
            $user_cache_key = "ms_graph_api_user_image_".md5($oid);
            $redis->set($user_cache_key, serialize($responseChecksum ));
            $redis->expire($user_cache_key, Settings::$cache_time_oid);
            $redis->set($img_cache_key, serialize($response));
            $redis->expire($img_cache_key, Settings::$cache_time_oid);
            return array ( "status"=>"OK","source"=>"LIVE","result"=> base64_encode($response) );
        } else {
        return array ( "status"=>"FAIL","reason"=>"ERR:".curl_getinfo($request, CURLINFO_HTTP_CODE),"http_status_code"=>curl_getinfo($request, CURLINFO_HTTP_CODE) );
        }
     } // end else if redis exists_key

} //end get getImage
function graph_getAllImageCacheKeys($myRedis=false) {
  if($myRedis!=false) { $redis=$myRedis;}
  if(!isset($redis)) { $redis = new Redis(); $redis->connect(Settings::$redis_host, Settings::$redis_port); }


  $userKeys=array();
  foreach ( $redis->keys('ms_graph_api_user_image_*') as $rediskey ) {
      $userKeys[str_replace('ms_graph_api_user_image_','',$rediskey)]=unserialize($redis->get($rediskey));
  }
  return json_encode($userKeys);
}

function graph_cacheUserImages() {
  return graph_getAllProfilePictures() ;
}
function graph_getAllProfilePictures() {
  if(!isset($redis)) { $redis = new Redis(); $redis->connect(Settings::$redis_host, Settings::$redis_port); }
  $statusArray=array();
  $users=graph_getAllUsers();
  //$guests=graph_getGuests();
  $token = AuthHelper::getDaemonToken();
  $handles = array();
  $process_count = 15;
//var_dump($guests);
//var_dump($guests["value"]);
//var_dump(array_chunk($guests["value"], 10));
  $chunkSize=10;
  $results=array();
  foreach( array_chunk($users["value"], $chunkSize, true) as $chunk ) {
    //foreach($chunk as $guest) {
      $mh = curl_multi_init();
      $handles = array();
      $process_count = $chunkSize;
      //while ($process_count--)
      foreach($chunk as $user) {
       {
         print "\nPREPARING IMAGE FOR ".$user["id"];
         $targetURL =
             Settings::$unifiedAPIEndpoint.'users/'.$user["id"].'/photo/$value';
         $request = curl_init();
         curl_setopt($request, CURLOPT_URL, $targetURL);
         //curl_setopt($request, CURLOPT_HEADER, 0);
         //curl_setopt($request, CURLOPT_RETURNTRANSFER, true);
         curl_setopt($request, CURLOPT_TIMEOUT, 30);
         curl_setopt($request, CURLOPT_HTTPHEADER, [
             "Authorization: Bearer " . $token->accessToken,
             //"Accept: application/json",
             //"Content-Type: image/jpeg",
             //  "Content-Type: text/plain"
         ]);
         //if($putORpatch==false) {
        //     curl_setopt($request, CURLOPT_CUSTOMREQUEST, 'PUT');
         //} else {
        //     curl_setopt($request, CURLOPT_CUSTOMREQUEST, 'PATCH');
         //}
         //curl_setopt($request, CURLOPT_POSTFIELDS, $fileContent );
         curl_setopt($request, CURLOPT_RETURNTRANSFER, true);
         curl_multi_add_handle($mh, $request);
         $handles[$user["id"]] = $request;
       } // end foreach guest

       $running=null;
       do
       {
           curl_multi_exec($mh, $running);
       } while ($running > 0);
       for($i = 0; $i < count($handles); $i++)
       foreach($handles as $handleID => $result)
       {

           $response = curl_multi_getcontent($handles[$handleID]);
           print  "\nRES FOR ". $handleID . " : " .md5($response);
           if (curl_getinfo($handles[$handleID], CURLINFO_HTTP_CODE) == "200") {
              // oh wow , something at MS works..
              $responseChecksum=md5($response);
              $img_cache_key="ms_graph_api_image_".$responseChecksum;
              $user_cache_key = "ms_graph_api_user_image_".md5($handleID);
              $redis->set($user_cache_key, serialize($responseChecksum ));
              $redis->expire($user_cache_key, Settings::$cache_time_oid);
              $redis->set($img_cache_key, serialize($response));
              $redis->expire($img_cache_key, Settings::$cache_time_oid);
              $statusArray[$handleID]=array ( "status"=>"OK","source"=>"LIVE","result"=> base64_encode($response) );
          } else {
              $statusArray[$handleID]=array ( "status"=>"FAIL","reason"=>"ERR:".curl_getinfo($request, CURLINFO_HTTP_CODE),"http_status_code"=>curl_getinfo($request, CURLINFO_HTTP_CODE) );
          }
           curl_multi_remove_handle($mh, $handles[$handleID]);
       }
    }
    curl_multi_close($mh);
    return $statusArray;
    //
    //print_r(graph_setImageFromFile($guest["id"],"/var/www/mydog-office365-adapter/guest.jpg"));
  } // end foreach chunk
}  // end graph_setAllGuestProfilePicturesToFile

function graph_getGuestUsersWithNoGroupMembership () {
    $allGuestsAsOID=array();
    $GuestIsInAnyGroup=array();
    $GuestUsersWithNoGroupMembership=array();
    $users = graph_getAllUsers();
    foreach ($users["value"] as $member) {
            if(graph_PrincipalIsExternal($member["userPrincipalName"])) {
                     //$membertype="Guest";
                     array_push($allGuestsAsOID,$member["id"]);
                     $GuestIsInAnyGroup[$member["id"]]=false;
            } else {
                     //$membertype="Member";
            }
    }
    $groups = graph_GetAllGroups();
    if (isset($groups) && !empty($groups) && isset($groups["value"] ) ) {
           foreach ($groups["value"] as $group) {
             $members=graph_getGroupByOID($group["id"]);
                 foreach ($members["value"] as $member) {
                    $GuestIsInAnyGroup[$member["id"]]=true;
                    } // end foreach members value
             } // end foreach groups value
    } //end groups isset
    foreach ( $allGuestsAsOID as $CurrentGuest) {
                  if( $GuestIsInAnyGroup[$CurrentGuest]==false) {
                     array_push(    $GuestUsersWithNoGroupMembership , $CurrentGuest);
                     }
                  }
     return $GuestUsersWithNoGroupMembership;
}

function graph_CreateGroup(

    $mailNickname,
    $ownerOID = null,
    $displayName = false,
    $description = false,
    $securityEnabled = false
) {

////POST https://graph.microsoft.com/v1.0/groups
////Content-Type: application/json
////
////{
////  "description": "Group with designated owner and members",
////  "displayName": "Operations group",
////  "groupTypes": [
////  ],
////  "mailEnabled": false,
////  "mailNickname": "operations2019",
////  "securityEnabled": true,
////  "owners@odata.bind": [
////    "https://graph.microsoft.com/v1.0/users/26be1845-4119-4801-a799-aea79d09f1a2"
////  ],
////  "members@odata.bind": [
////    "https://graph.microsoft.com/v1.0/users/ff7cb387-6688-423c-8188-3da9532a73cc",
////    "https://graph.microsoft.com/v1.0/users/69456242-0067-49d3-ba96-9de6f2728e14"
////  ]
////}

    if ($displayName == false) {
        $myname = $mailNickname;
    } else {
        $myname = $displayName;
    }
    if ($description == false) {
        $description = $mailNickname;
    } else {
        $description = $description;
    }
    $graphTemplate_CreateGroup = [
        "mailEnabled" => true,
        "displayName" => $myname,
        "mailNickname" => $mailNickname,
        "groupTypes" => array("Unified")
    ];

    if (isset($securityEnabled) && $securityEnabled) {
        $graphTemplate_CreateGroup["securityEnabled"] = true;
    } else {
        $graphTemplate_CreateGroup["securityEnabled"] = false;
    }
    ///////////////


    if ($ownerOID == null) {
        $graphTemplate_CreateGroup["owners@odata.bind"] = [
            "https://graph.microsoft.com/v1.0/users/" .
            graph_getOID_byMail_OR_Principal(
                strtolower(Settings::$tenantADMINmail) .
                    "@" .
                    strtolower(Settings::$tenantDOMAIN)
            ),
        ];
    } else {
        $graphTemplate_CreateGroup["owners@odata.bind"] = [
            "https://graph.microsoft.com/v1.0/users/" . $ownerOID,
        ];
    }

    //var_dump($graphTemplate_CreateGroup);
    $payload = json_encode($graphTemplate_CreateGroup);
    $token = AuthHelper::getDaemonToken();
    $targetURL = Settings::$unifiedAPIEndpoint . "groups";
    $request = curl_init($targetURL);
    curl_setopt($request, CURLOPT_HTTPHEADER, [
        "Authorization: Bearer " . $token->accessToken,
        "Accept: application/json",
        "Content-Type:application/json",
    ]);
    curl_setopt($request, CURLOPT_POST, true);
    curl_setopt($request, CURLOPT_POSTFIELDS, $payload);
    curl_setopt($request, CURLOPT_RETURNTRANSFER, true);
    //echo '<script>console.log(\' REQ: '.print_r($request).'\');</script>';
    $response = curl_exec($request);
    //echo '<script>console.log(\' RES: '.print_r($response).'\');</script>';
    //parse the json into oci_fetch_object
    //$result = json_decode($response, true);
    //return $result;
    //return json_decode($response, true);
    //api docs say 201


    if (curl_getinfo($request, CURLINFO_HTTP_CODE) > 199 && curl_getinfo($request, CURLINFO_HTTP_CODE) < 300 ) {
            //graph API OK direct
            return array ( "status"=>"OK","result"=>json_decode($response, true));
        } else {
            // a non-group-member cannot be removed again
            // return OK in that case as well
            //parse the json into oci_fetch_object
            $result = json_decode($response, true);
			//if(isset ($response["error"])  && isset ($response["error"]["message"]) && strpos($response["error"]["message"],'does not exist or one of its queried reference-property objects are not present') != false)  {
			if(isset ($response["error"])    && isset ($response["error"]["message"]) && strpos($response["error"]["message"],'Another object with the same value for property mailNickname already exists') != false) {
             return array ( "status"=>"OK","result"=>$result);
			     } else {
				      //failed for real
				      return array ( "status"=>"FAIL","http_status_code"=>curl_getinfo($request, CURLINFO_HTTP_CODE),"result"=>$response);
				    }  // end else errormessage says request is okay (softfail) but users were not in group so made 1  bogus request
			} // end else response not 201
}

function graph_GetRiskyUsers()
{
    https: //graph.microsoft.com/
    $token = AuthHelper::getDaemonToken();
    //perform a REST query for the users modern groups
    $request = curl_init(
        Settings::$unifiedAPIEndpoint .
            "/identityProtection/riskyUsers?" .
            '$filter=riskLevel eq ' .
            "'high'"
    );
    curl_setopt($request, CURLOPT_HTTPHEADER, [
        "Authorization: Bearer " . $token->accessToken,
        "Accept: application/json",
    ]);
    curl_setopt($request, CURLOPT_RETURNTRANSFER, true);
    $response = curl_exec($request);

    //parse the json into oci_fetch_object
    $members = json_decode($response, true);

    $risks = json_decode($response, true);
    if (isset($risks) && !empty($risks) && !isset($risks["error"]["code"])) {
        return $risks;
        // file_put_contents("/tmp/ADgroups.json.tmp",$response);
    } else {
        $retme = [];
        return $retme;
    }
}

function graph_LicenseUserSettingsURLFromOID($oid)
{
    return "https://admin.microsoft.com/adminportal/home?#/users/:/UserDetails/" .
        $oid .
        "/LicensesAndApps";
}

function graph_ExternalDomainPrincipalForMail($mail) {
  return str_replace("@", "_",$mail)."#EXT#@".Settings::$tenantDOMAIN ;
}

function graph_PrincipalIsExternal($principal)
{
    // example would be username_userdomain.tld#EXT#@M365x57871887.onmicrosoft.com
    if (strstr($principal, "#EXT#@")) {
        return true; //echo "Text found";
    } else {
        return false; //echo "Text not found";
    }
}


function graph_CacheUsersFromArray(array $members, $debugThis = false)
{
    if (isset($members["value"]) && isset($members["@odata.context"])) {
        $members = $members["value"];
    }
    if ($debugThis) {
        var_dump($members);
    }
    if(!isset($redis)) { $redis = new Redis(); $redis->connect(Settings::$redis_host, Settings::$redis_port); }


    foreach ($members as $member) {
        //lowercase principal and mail first
        if (isset($member["mail"]) && $member["mail"] != null) {
            $member["mail"] = strtolower($member["mail"]);
        }
        if (isset($member["userPrincipalName"])) {
            $member["userPrincipalName"] = strtolower(
                $member["userPrincipalName"]
            );
        }
        if (isset($member["id"]) && $member["id"] != null) {
            $identifier = $member["id"];
            $cache_key = "ms_graph_api_user_oid_" . $identifier;
            ///var_dump($member);$redis->expire($cache_key, 1);
            if ($redis->exists($cache_key)) {
                if (Settings::$debugAppVerbose) {
                    echo "\nfromcache_oid" . $member["id"];
                }
            } else {
                if (Settings::$debugAppVerbose) {
                    echo "\nTOcache_oid KEY:".$cache_key."VAL: (memberjson)";
                }
                $redis->set($cache_key, serialize(json_encode($member)));
                $redis->expire($cache_key, Settings::$cache_time_oid);
            }
        }
        if (isset($member["displayName"]) && $member["displayName"] != null) {
            $identifier = $member["id"];
            $cache_key = "ms_graph_api_user_displayName_" . md5($identifier);
            //$redis->expire($cache_key, 1);
            if ($redis->exists($cache_key)) {
                if (Settings::$debugAppVerbose) {
                    echo "\nfromcache_oid" . $member["id"];
                }
            } else {
                if (Settings::$debugAppVerbose) {
                    echo "\nTOcache_oid KEY:".$cache_key."VAL: (memberjson)";
                }
                $redis->set($cache_key, serialize(json_encode($member)));
                $redis->expire($cache_key, Settings::$cache_time_oid);
            }
        }
        // now the optional stuff, but they all should have userPrincipalName (UPN)
        foreach( array("displayName","givenName","surname","jobTitle","externalUserState") as $currentKey ) {
            if (isset($member[$currentKey]) && $member[$currentKey] != null) {
                $identifier = $member[$currentKey];
                $cache_key = "ms_graph_api_user_displayName_" . md5($identifier);
                //$redis->expire($cache_key, 1);
                if ($redis->exists($cache_key)) {
                    if (Settings::$debugAppVerbose) {
                        echo "\nfromcache_oid_FORCING_UPDATE_".$currentKey.":". $member["id"];
                    }
                    $redis->set($cache_key, serialize(json_encode($member)));
                    $redis->expire($cache_key, Settings::$cache_time_oid);
                } else {
                    $redis->set($cache_key, serialize(json_encode($member)));
                    $redis->expire($cache_key, Settings::$cache_time_oid);
                } // ende else redis exists
            } //end isset

        } // end foreach currentkey
        if (isset($member["mail"]) && $member["mail"] != null) {
            $identifier = $member["mail"];
            $cache_key   = "ms_graph_api_user_mail_" . md5($identifier);
            $cache_keyLC = "ms_graph_api_user_mail_" . md5(strtolower($identifier));
            //$redis->expire($cache_key, 1);
            if ($redis->exists($cache_key)) {
                if (Settings::$debugAppVerbose) {
                    echo "\nfromcache_mail_FORCING_UPDATE_".$currentKey.":". $member["id"];
                }
                $redis->set($cache_key, serialize(json_encode($member)));
                $redis->expire($cache_key, Settings::$cache_time_oid);
            } else {
                if (Settings::$debugAppVerbose) {
                    echo "\nTOcache_oid KEY:".$cache_key."VAL:". $member["id"];
                }
                //first the blank one
                $redis->set($cache_key, serialize(json_encode($member)));
                $redis->expire($cache_key, Settings::$cache_time_oid);
                // is the mail the same in lower and uppercase ? if not set this as well
                if( $cache_key != $cache_keyLC ) {
                  $redis->set($cache_keyLC, serialize(json_encode($member)));
                  $redis->expire($cache_keyLC, Settings::$cache_time_oid);
                 }
                // is the user mail from our tenant domain ?
                if( strtolower(domainOfMailAdress($member["mail"])) == strtolower(Settings::$tenantDOMAIN) ) {
                   // does the domain UpperLowerCase of the mail differ from our tenantdomain ? if so , store them lowercase and MixedCase
                   if( domainOfMailAdress($member["mail"]) != Settings::$tenantDOMAIN ) {
                        $identifier= userOfMailAddress($member["mail"])."@".Settings::$tenantDOMAIN;
                        $cache_key   = "ms_graph_api_user_mail_" . md5($identifier);
                        $redis->set($cache_key, serialize(json_encode($member)));
                        $redis->expire($cache_key, Settings::$cache_time_oid);
					    }
                    } // end  if( strtolower(domainOfMailAdress($target)) == strtolower(Settings::$tenantDOMAIN) )

           } // end if_else ($redis->exists($cache_key))
        } // if member has mail element

        if (isset($member["userPrincipalName"])) {
            $identifier = $member["userPrincipalName"];
            $cache_key = "ms_graph_api_user_principal_" . md5($identifier);
            //$redis->expire($cache_key, 1);
            if ($redis->exists($cache_key)) {
                    if (Settings::$debugAppVerbose) {
                        echo "\nfromcache_UPN_FORCING_UPDATE_".$currentKey.":". $member["id"];
                    }
                    $redis->set($cache_key, serialize(json_encode($member)));
                    $redis->expire($cache_key, Settings::$cache_time_oid);
            } else {
                if (Settings::$debugAppVerbose) {
                    echo "\nTOcache_UPN KEY:".$cache_key."VAL:". $member["id"];
                }
				$memberJSON=json_encode($member);
                $redis->set($cache_key, serialize($memberJSON));
                $redis->expire($cache_key, Settings::$cache_time_oid);
                //store the lowecase UPN as well since MS Graph API goes crazy without sanitization ( no oid search results by UPN)
                $possibleUPN=array();
                foreach ( array(  "ms_graph_api_user_principal_" . md5($member["userPrincipalName"]),
                                  "ms_graph_api_user_principal_" . md5(strtolower($member["userPrincipalName"]) ),
                                  "ms_graph_api_user_principal_" . md5(userOfMailAddress($member["userPrincipalName"]."@".Settings::$tenantDOMAIN ) )
                               ) as $local_cache_key) {
						if ( !in_array($local_cache_key,$possibleUPN)) {
							array_push($possibleUPN,$local_cache_key);
							}

				  } //end foreach $local_cache_key

				 foreach (array_unique($possibleUPN) as $mycachekey) {
					     $redis->set($mycachekey, serialize($memberJSON));
                         $redis->expire($mycachekey, Settings::$cache_time_oid);
				 } //end foreach $possibleUPN
            } //end if redis_key_exists
        } //if member has UPN
    } //end foreach members as member
} // end function graph_CacheUsersFromArray


function graph_getMailAdress_byOID($identifier)
{
    if(!isset($redis)) { $redis = new Redis(); }
    $redis->connect(Settings::$redis_host, Settings::$redis_port);
    $cache_key = "ms_graph_api_user_oid_" . $identifier;
    //$redis->expire($cache_key, 1);
    if (Settings::$debugAppVerbose) {
        var_dump($cache_key);
    }
    if ($redis->exists($cache_key)) {
        $resp = json_decode(unserialize($redis->get($cache_key)), true);
        if (Settings::$debugAppVerbose) {
            var_dump($resp);
        }

        if (isset($resp["mail"])) {
            return $resp["mail"];
        } else {
            graph_CacheUsersFromArray(graph_getAllUsers(false));
            $resp = json_decode(unserialize($redis->get($cache_key)), true);
            if (isset($resp["mail"])) {
                return $resp["mail"];
            } else {
                return "";
                if (Settings::$debugAppVerbose) {
                    print_r(json_encode($resp));
                }
            }
        }
    } else {
        graph_CacheUsersFromArray(graph_getAllUsers(false));
        $resp = json_decode(unserialize($redis->get($cache_key)), true);
        if (isset($resp["mail"])) {
            return $resp["mail"];
        } else {
            return "";
        }
    }
}



//this is the main search OID function usable also for userPrincipalName
function graph_getOID_byMail(
    $mail,
    $searchPrincipal = false,
    $principalOnly = false
) {
    if(!isset($redis)) { $redis = new Redis(); }
    $redis->connect(Settings::$redis_host, Settings::$redis_port);
    $searchscopes=array();
    if ($searchPrincipal) {
        if ($principalOnly) {
			if($mail != strtolower($mail) ) {
				 $searchscopes = ["ms_graph_api_user_principal_" . md5($mail), "ms_graph_api_user_principal_" . md5(strtolower($mail)) ];
			} else {
                 $searchscopes = ["ms_graph_api_user_principal_" . md5($mail)];
            } // end ifelse strtolower


        } else { // if_else principalonly
			if($mail != strtolower($mail) ) {     // search for UpperCase and lowercase
                $searchscopes = [
                    "ms_graph_api_user_mail_" . md5($mail),
                    "ms_graph_api_user_principal_" . md5($mail),
                    "ms_graph_api_user_mail_" . md5(strtolower($mail)),
                    "ms_graph_api_user_principal_" . md5(strtolower($mail)),
                ];
			} else {
                $searchscopes = [
                    "ms_graph_api_user_mail_" . md5($mail),
                    "ms_graph_api_user_principal_" . md5($mail),
                ];
		    }
            //FIX MS LOWER_UPPERCASE_STRANGENESS FOR MAIL ONLY
                // on tenant domain but lowercase
                if(
                    ( strtolower( domainOfMailAdress($mail)) == strtolower(Settings::$tenantDOMAIN)  ) &&
                    ( strtolower( domainOfMailAdress($mail)) != domainOfMailAdress($mail) )
                  ) {
                   array_push( $searchscopes, "ms_graph_api_user_mail_" . md5( userOfMailAddress($mail)."@".Settings::$tenantDOMAIN ) );
                 }

        } // end if_else principalonly
            //FIX MS LOWER_UPPERCASE_STRANGENESS FOR PRINCIPAL
                // on tenant domain but lowercase
                if(
                    ( strtolower( domainOfMailAdress($mail)) == strtolower(Settings::$tenantDOMAIN)  ) &&
                    ( strtolower( domainOfMailAdress($mail)) != domainOfMailAdress($mail) )
                  ) {
                   array_push( $searchscopes, "ms_graph_api_user_principal_" . md5( userOfMailAddress($mail)."@".Settings::$tenantDOMAIN ) );
                 }

    } else { // else if_else searchprincipal
        //search by mail
			if($mail != strtolower($mail) ) {     // search for UpperCase and lowercase
				 $searchscopes = ["ms_graph_api_user_mail_" . md5($mail), "ms_graph_api_user_mail_" . md5(strtolower($mail)) ];
			} else {
                 $searchscopes = ["ms_graph_api_user_mail_" . md5($mail)];
            } // end ifelse strtolower
            //FIX MS LOWER_UPPERCASE_STRANGENESS FOR MAIL ONLY
                // on tenant domain but lowercase
                if(
                    ( strtolower( domainOfMailAdress($mail)) == strtolower(Settings::$tenantDOMAIN)  ) &&
                    ( strtolower( domainOfMailAdress($mail)) != domainOfMailAdress($mail) )
                  ) {
                   array_push( $searchscopes, "ms_graph_api_user_mail_" . md5( userOfMailAddress($mail)."@".Settings::$tenantDOMAIN ) );
                 }
    }  // else if_else searchprincipal


//search runs in main function context
             if (Settings::$debugAppVerbose) {
                 echo "\nDBG::SEARCH:SCOPE_DUMP: \n NEEDLE: ".$mail." \n NEEDLE_SCOPES: \n ".json_encode($searchscopes)."\n" ;
             }

             $resultfound=false;
             foreach (array_unique($searchscopes) as $cache_key) {
                 if ($redis->exists($cache_key)) {
                     $resp = json_decode(unserialize($redis->get($cache_key)), true);
                     if (isset($resp["id"])) {
                         return $resp["id"];
                         $resultfound=true;
	         		    }
                 } else {
                     // cache not found
                     graph_CacheUsersFromArray(graph_getAllUsers(false));
                     $resp = json_decode(unserialize($redis->get($cache_key)), true);
                     if (isset($resp["id"])) {
                         return $resp["id"];
                         $resultfound=true;
                     }

                 } // end no cache
             } // end foreach

             if($resultfound==false) {
            return "";
             }
} //end function graph_getOID_byMail(




function graph_getOID_byMail_OR_Principal($mail)
{
    return graph_getOID_byMail($mail, true); //search for principal AS WELL
}

function graph_getOID_byPrincipal($mail)
{
    return graph_getOID_byMail($mail, true, true); //search for principal ONLY
}

function graph_getUserByMail_OR_Principal($id)
{
    return graph_getUserByOID(graph_getOID_byMail_OR_Principal($id));
}

function graph_getUserByOID($oid)
{
    $cache_key = "ms_graph_api_user_oid_" . $oid;
    if(!isset($redis)) { $redis = new Redis(); }
    $redis->connect(Settings::$redis_host, Settings::$redis_port);
    if (Settings::$debugAppVerbose) {
        var_dump($cache_key);
    }
    if ($redis->exists($cache_key)) {
        $resp = json_decode(unserialize($redis->get($cache_key)), true);
        if (Settings::$debugAppVerbose) {
            var_dump($resp);
        }

        if (isset($resp["userPrincipalName"])) {
            return $resp;
        } else {
            // the cached object seems defect
            graph_CacheUsersFromArray(graph_getAllUsers(false));
            $resp = json_decode(unserialize($redis->get($cache_key)), true);
            if (isset($resp["userPrincipalName"])) {
                return $resp;
            } else {
                $retme = [];
                return $retme;
            }
        }
    } else {
        // cache not found
        graph_CacheUsersFromArray(graph_getAllUsers(false));
        $resp = json_decode(unserialize($redis->get($cache_key)), true);
        if (isset($resp["userPrincipalName"])) {
            return $resp;
        } else {
            $retme = [];
            return $retme;
        }
    }
}

function graph_getGuests($useCache = true)
{
    if(!isset($redis)) { $redis = new Redis(); }
    $redis->connect(Settings::$redis_host, Settings::$redis_port);
    $cache_key = "ms_graph_api_guestlist";
    if ($redis->exists($cache_key) && $useCache == true) {
        ///  // $data_source = "Data from Redis Server";
        $users = [];
        $users = unserialize($redis->get($cache_key));
        return $users;
    } else {
        $token = AuthHelper::getDaemonToken();
        //$targetURL = Settings::$unifiedAPIEndpoint . 'users?$filter=UserType eq '."'".'Guest'."'".'&$select=externalUserState,id,jobTitle,surname,givenName,userPrincipalName,mail&$top=999';
        $targetURL = Settings::$unifiedAPIEndpoint . 'users?$filter=UserType%20eq%20%27Guest%27&$select=externalUserState,id,jobTitle,surname,givenName,userPrincipalName,mail&$top=999';
        // e.g.. // https://graph.microsoft.com/v1.0/users?$filter=UserType eq 'Guest'&$select=externalUserState,id,jobTitle,surname,givenName,userPrincipalName,mail&$top=999
        $request = curl_init($targetURL);
        curl_setopt($request, CURLOPT_HTTPHEADER, [
            "Authorization: Bearer " . $token->accessToken,
            "Accept: application/json",
        ]);
        curl_setopt($request, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($request);
        //print_r($targetURL);
        //echo '<script>console.log(\' GUESTS RES: '.$response.'\');</script>';
        //parse the json into oci_fetch_object
        //var_dump($response);
        $users = json_decode($response, true);
        if (
            isset($users) &&
            !empty($users) &&
            !isset($users["error"]["code"])
        ) {
            //echo '<code>'
            //print json_encode($response,	JSON_PRETTY_PRINT)."\n";
            //echo '</code>';
            $apiRoot = $users["@odata.context"];
            //$apiRoot = substr($apiRoot, 0, strrpos($apiRoot, "/"));
            //$_SESSION[Settings::$apiRoot] = $apiRoot;
            $redis->set($cache_key, serialize($users));
            $redis->expire($cache_key, Settings::$cache_time_groups);
            graph_CacheGuestsFromArray($users);
            return $users;
            // file_put_contents("/tmp/ADgroups.json.tmp",$response);
        } else {
            $retme = [];
            return $retme;
        }
    }
} //end get guests


function graph_setProfilePictureFromFile($oid,$fileLocation,$putORpatch=false) {
  if(!isset( $oid)) {
      return false;
    }

  $dumpArgs=false;
  //  if (Settings::$debugAppVerbose   || Settings::$debugApp|| Settings::$debugAppMail ) { $dumpArgs=true;  }
  if (Settings::$debugAppVerbose   ) { $dumpArgs=true;  }
  if(file_exists($fileLocation)) {
    $fileContent=file_get_contents($fileLocation);
    $token = AuthHelper::getDaemonToken();
    $targetURL =
        Settings::$unifiedAPIEndpoint.'users/'.$oid.'/photo/$value';
    //	 var_dump($targetURL);
    $request = curl_init($targetURL);
    curl_setopt($request, CURLOPT_HTTPHEADER, [
        "Authorization: Bearer " . $token->accessToken,
        "Accept: application/json",
        "Content-Type: image/jpeg",
        //  "Content-Type: text/plain"
    ]);

    //curl_setopt($request, CURLOPT_POST, true);

    //FAILS: curl_setopt($request	, CURLOPT_PATCH, true);
    //curl_setopt($request, CURLOPT_CUSTOMREQUEST, 'PATCH');
    if($putORpatch==false) {
        curl_setopt($request, CURLOPT_CUSTOMREQUEST, 'PUT');
    } else {
        curl_setopt($request, CURLOPT_CUSTOMREQUEST, 'PATCH');
    }

    if($dumpArgs) {
      echo "send PHOTOUPDATE TO ".$targetURL;
        var_dump($messageJSON);
    }

    curl_setopt($request, CURLOPT_POSTFIELDS, $fileContent );
    curl_setopt($request, CURLOPT_RETURNTRANSFER, true);
    $response = curl_exec($request);
    return $response;
  } else { return false; }



} // end graph_setImageFromFile


function graph_setAllGuestProfilePicturesFromFile($fileLocation ,$putORpatch=false) {
  if(file_exists($fileLocation)) {
  $fileContent=file_get_contents($fileLocation);
  $guests=graph_getGuests();
  $token = AuthHelper::getDaemonToken();
  $handles = array();
  $process_count = 15;
//var_dump($guests);
//var_dump($guests["value"]);
//var_dump(array_chunk($guests["value"], 10));
  $chunkSize=10;
  $results=array();
  foreach( array_chunk($guests["value"], $chunkSize, true) as $chunk ) {
    //foreach($chunk as $guest) {
      $mh = curl_multi_init();
      $handles = array();
      $process_count = $chunkSize;
      //while ($process_count--)
      foreach($chunk as $guest) {
       {
         print "\nPREPARING IMAGE UPLd FOR ".$guest["id"];
         $targetURL =
             Settings::$unifiedAPIEndpoint.'users/'.$guest["id"].'/photo/$value';
         $request = curl_init();
         curl_setopt($request, CURLOPT_URL, $targetURL);
         //curl_setopt($request, CURLOPT_HEADER, 0);
         //curl_setopt($request, CURLOPT_RETURNTRANSFER, true);
         curl_setopt($request, CURLOPT_TIMEOUT, 30);
         curl_setopt($request, CURLOPT_HTTPHEADER, [
             "Authorization: Bearer " . $token->accessToken,
             "Accept: application/json",
             "Content-Type: image/jpeg",
             //  "Content-Type: text/plain"
         ]);
         if($putORpatch==false) {
             curl_setopt($request, CURLOPT_CUSTOMREQUEST, 'PUT');
         } else {
             curl_setopt($request, CURLOPT_CUSTOMREQUEST, 'PATCH');
         }
         curl_setopt($request, CURLOPT_POSTFIELDS, $fileContent );
         curl_setopt($request, CURLOPT_RETURNTRANSFER, true);
         curl_multi_add_handle($mh, $request);
         $handles[$guest["id"]] = $request;
       } // end foreach guest

       $running=null;
       do
       {
           curl_multi_exec($mh, $running);
       } while ($running > 0);
       for($i = 0; $i < count($handles); $i++)
       foreach($handles as $handleID => $result)
       {
           $out = curl_multi_getcontent($handles[$handleID]);
           print  "\nRES FOR ". $handleID . " : " .$out;
           curl_multi_remove_handle($mh, $handles[$handleID]);
       }
    }
    curl_multi_close($mh);

    //
    //print_r(graph_setImageFromFile($guest["id"],"/var/www/mydog-office365-adapter/guest.jpg"));
  } // end foreach chunk
} // end if location ecists
}  // end graph_setAllGuestProfilePicturesToFile

function graph_getAllGroups($useCache = true)
{
    if(!isset($redis)) { $redis = new Redis(); }
    $redis->connect(Settings::$redis_host, Settings::$redis_port);
    //$cache_key = md5($sql);
    $cache_key = "ms_graph_api_grouplist";
    if ($redis->exists($cache_key) && $useCache == true) {
        ///  // $data_source = "Data from Redis Server";
        $groups = [];
        $groups = unserialize($redis->get($cache_key));
        return $groups;
    } else {
        $token = AuthHelper::getDaemonToken();
        $targetURL = Settings::$unifiedAPIEndpoint . "groups?" . '$top=999';
        $request = curl_init($targetURL);
        curl_setopt($request, CURLOPT_HTTPHEADER, [
            "Authorization: Bearer " . $token->accessToken,
            "Accept: application/json",
        ]);
        curl_setopt($request, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($request);
        //echo '<script>console.log(\' RES: '.$response.'\');</script>';
        //parse the json into oci_fetch_object
        $groups = json_decode($response, true);
        if (
            isset($groups) &&
            !empty($groups) &&
            !isset($groups["error"]["code"])
        ) {
            //echo '<code>'
            //print json_encode($response,	JSON_PRETTY_PRINT)."\n";
            //echo '</code>';
            // $apiRoot = $groups["@odata.context"];
            //$apiRoot = substr($apiRoot, 0, strrpos($apiRoot, "/"));
            //$_SESSION[Settings::$apiRoot] = $apiRoot;
            $redis->set($cache_key, serialize($groups));
            $redis->expire($cache_key, Settings::$cache_time_groups);
            foreach ($groups["value"] as $group) {
                if (isset($group["mail"]) && $group["mail"] != null) {
                    $group["mail"] = strtolower($group["mail"]);
                }
                if (isset($group["id"]) && $group["id"] != null) {
                    $identifier = $group["id"];
                    $cache_key = "ms_graph_api_user_oid_" . $identifier;
                    if (Settings::$debugAppVerbose) {
                        var_dump($group);
                        $redis->expire($cache_key, 1);
                    }

                    ///
                    if ($redis->exists($cache_key)) {
                        if (Settings::$debugAppVerbose) {
                            echo "\nfromcache_oid" . $group["id"];
                        }
                    } else {
                        $redis->set($cache_key, serialize(json_encode($group)));
                        $redis->expire($cache_key, Settings::$cache_time_oid);
                    }
                }
                if (isset($group["mail"]) && $group["mail"] != null) {
                    $identifier = $group["mail"];
                    $cache_key = "ms_graph_api_user_mail_" . md5($identifier);
                    //$redis->expire($cache_key, 1);
                    if ($redis->exists($cache_key)) {
                        if (Settings::$debugAppVerbose) {
                            echo "\nfromcache_oid" . $group["id"];
                        }
                    } else {
                        $redis->set($cache_key, serialize(json_encode($group)));
                        $redis->expire($cache_key, Settings::$cache_time_oid);
                    }
                }
            }
            return $groups;
            // file_put_contents("/tmp/ADgroups.json.tmp",$response);
        } else {
            $retme = [];
            return $retme;
        }
    }
}

/////

function graph_getGroupByMail($mail, $useCache = true)
{
    $groups = graph_getAllGroups();
    if (isset($groups) && !empty($groups)) {
        foreach ($groups["value"] as $group) {
            //  echo "\n<br> | need ".strtolower($mail)." have ".strtolower($group["mail"])." |";

            if (strtolower($group["mail"]) == strtolower($mail)) {
                if (isset($group["mail"])) {
                    if (Settings::$debugAppVerbose) {
                        echo '<script>console.log(\' GROUP:' .
                            json_encode($group) .
                            '\');</script>';
                    }
                    return $group;
                } else {
                    return false;
                }
            }
        } //end foreach
        return false;
    } else {
        return false;
    }
}

function graph_getGroupMail($oid, $useCache = true)
{
    $groups = graph_getAllGroups();
    if (isset($groups) && !empty($groups)) {
        foreach ($groups["value"] as $group) {
            if ($group["id"] == $oid) {
                if (isset($group["mail"])) {
                    if (Settings::$debugAppVerbose) {
                        echo '<script>console.log(\' GROUP:' .
                            json_encode($group) .
                            '\');</script>';
                    }
                    return $group["$mail"];
                } else {
                    return false;
                }
            }
        } //end foreach
        return false;
    } else {
        return false;
    }
}

function graph_getGroupByOID($id, $useCache = true)
{
    if(!isset($redis)) { $redis = new Redis(); }
    $redis->connect(Settings::$redis_host, Settings::$redis_port);
    //$cache_key = md5($sql);
    $cache_key = "ms_graph_api_group_" . $id;
    if ($redis->exists($cache_key) && $useCache == true) {
        ///  // $data_source = "Data from Redis Server";
        $groups = [];
        $groups = unserialize($redis->get($cache_key));
        return $groups;
    } else {
        $token = AuthHelper::getDaemonToken();
        //perform a REST query for the users modern groups
        $request = curl_init(
            Settings::$unifiedAPIEndpoint . "/groups/" . $id . '/members?$top=999'
        );
        curl_setopt($request, CURLOPT_HTTPHEADER, [
            "Authorization: Bearer " . $token->accessToken,
            "Accept: application/json",
        ]);
        curl_setopt($request, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($request);

        //parse the json into oci_fetch_object
        $members = json_decode($response, true);

        $group = json_decode($response, true);
        if (
            isset($group) &&
            !empty($group) &&
            !isset($group["error"]["code"])
        ) {
            //echo '<code>'
            //print json_encode($response,	JSON_PRETTY_PRINT)."\n";
            //echo '</code>';
            // $apiRoot = $groups["@odata.context"];
            //$apiRoot = substr($apiRoot, 0, strrpos($apiRoot, "/"));
            //$_SESSION[Settings::$apiRoot] = $apiRoot;
            $redis->set($cache_key, serialize($group));
            $redis->expire($cache_key, Settings::$cache_time_groups);

            return $group;
            // file_put_contents("/tmp/ADgroups.json.tmp",$response);
        } else {
            $retme = [];
            return $retme;
        }
    }
}

///////



function graph_CacheGuestsFromArray(array $members, $debugThis = false)
{
    if (isset($members["value"]) && isset($members["@odata.context"])) {
        $members = $members["value"];
    }
    if ($debugThis) {
        var_dump($members);
    }
    if(!isset($redis)) { $redis = new Redis(); }
    $redis->connect(Settings::$redis_host, Settings::$redis_port);

    foreach ($members as $member) {
        /////lowercase principal and mail first
        ///if (isset($member["mail"]) && $member["mail"] != null) {
        ///    $member["mail"] = strtolower($member["mail"]);
        ///}
        ///if (isset($member["userPrincipalName"])) {
        ///    $member["userPrincipalName"] = strtolower(
        ///        $member["userPrincipalName"]
        ///    );
        ///}
        if (isset($member["id"]) && $member["id"] != null) {
            $identifier = $member["id"];
            $cache_key = "ms_graph_api_user_oid_" . $identifier;
            ///var_dump($member);$redis->expire($cache_key, 1);
            if ($redis->exists($cache_key)) {
                if (Settings::$debugAppVerbose) {
                    echo "\nfromcache_oid_FORCING_UPDATE" . $member["id"];
                }
                $redis->set($cache_key, serialize(json_encode($member)));
                $redis->expire($cache_key, Settings::$cache_time_oid);
            } else {
                $redis->set($cache_key, serialize(json_encode($member)));
                $redis->expire($cache_key, Settings::$cache_time_oid);
            }
        }
       // now the optional stuff, but they all should have userPrincipalName (UPN)
        foreach( array("displayName","givenName","surname","jobTitle","externalUserState") as $currentKey ) {
            if (isset($member[$currentKey]) && $member[$currentKey] != null) {
                $identifier = $member[$currentKey];
                $cache_key = "ms_graph_api_user_displayName_" . md5($identifier);
                //$redis->expire($cache_key, 1);
                if ($redis->exists($cache_key)) {
                    if (Settings::$debugAppVerbose) {
                        echo "\nfromcache_oid_FORCING_UPDATE_".$currentKey.":". $member["id"];
                    }
                    $redis->set($cache_key, serialize(json_encode($member)));
                    $redis->expire($cache_key, Settings::$cache_time_oid);
                } else {
                    $redis->set($cache_key, serialize(json_encode($member)));
                    $redis->expire($cache_key, Settings::$cache_time_oid);
                } // ende else redis exists
            } //end isset

        } // end foreach currentkey
        if (isset($member["mail"]) && $member["mail"] != null) {
            $identifier = $member["mail"];
            $cache_key   = "ms_graph_api_user_mail_" . md5($identifier);
            $cache_keyLC = "ms_graph_api_user_mail_" . md5(strtolower($identifier));
            //$redis->expire($cache_key, 1);
            if ($redis->exists($cache_key)) {
                if (Settings::$debugAppVerbose) {
                    echo "\nfromcache_mail_FORCING_UPDATE_".$currentKey.":". $member["id"];
                }
                $redis->set($cache_key, serialize(json_encode($member)));
                $redis->expire($cache_key, Settings::$cache_time_oid);
            } else {
                if (Settings::$debugAppVerbose) {
                    echo "\nTOcache_oid KEY:".$cache_key."VAL:". $member["id"];
                }
                //first the blank one
                $redis->set($cache_key, serialize(json_encode($member)));
                $redis->expire($cache_key, Settings::$cache_time_oid);
                // is the mail the same in lower and uppercase ? if not set this as well
                if( $cache_key != $cache_keyLC ) {
                  $redis->set($cache_keyLC, serialize(json_encode($member)));
                  $redis->expire($cache_keyLC, Settings::$cache_time_oid);
                 }
                // is the user mail from our tenant domain ?
                if( strtolower(domainOfMailAdress($member["mail"])) == strtolower(Settings::$tenantDOMAIN) ) {
                   // does the domain UpperLowerCase of the mail differ from our tenantdomain ? if so , store them lowercase and MixedCase
                   if( domainOfMailAdress($member["mail"]) != Settings::$tenantDOMAIN ) {
                        $identifier= userOfMailAddress($member["mail"])."@".Settings::$tenantDOMAIN;
                        $cache_key   = "ms_graph_api_user_mail_" . md5($identifier);
                        $redis->set($cache_key, serialize(json_encode($member)));
                        $redis->expire($cache_key, Settings::$cache_time_oid);
					    }
                    } // end  if( strtolower(domainOfMailAdress($target)) == strtolower(Settings::$tenantDOMAIN) )

           } // end if_else ($redis->exists($cache_key))
        } // if member has mail element

        if (isset($member["userPrincipalName"])) {
            $identifier = $member["userPrincipalName"];
            $cache_key = "ms_graph_api_user_principal_" . md5($identifier);
            //$redis->expire($cache_key, 1);
            if ($redis->exists($cache_key)) {
                    if (Settings::$debugAppVerbose) {
                        echo "\nfromcache_UPN_FORCING_UPDATE_".$currentKey.":". $member["id"];
                    }
                    $redis->set($cache_key, serialize(json_encode($member)));
                    $redis->expire($cache_key, Settings::$cache_time_oid);
            } else {
                if (Settings::$debugAppVerbose) {
                    echo "\nTOcache_UPN KEY:".$cache_key."VAL:". $member["id"];
                }
				$memberJSON=json_encode($member);
                $redis->set($cache_key, serialize($memberJSON));
                $redis->expire($cache_key, Settings::$cache_time_oid);
                //store the lowecase UPN as well since MS Graph API goes crazy without sanitization ( no oid search results by UPN)
                $possibleUPN=array();
                foreach ( array(  "ms_graph_api_user_principal_" . md5($member["userPrincipalName"]),
                                  "ms_graph_api_user_principal_" . md5(strtolower($member["userPrincipalName"]) ),
                                  "ms_graph_api_user_principal_" . md5(userOfMailAddress($member["userPrincipalName"]."@".Settings::$tenantDOMAIN ) )
                               ) as $local_cache_key) {
						if ( !in_array($local_cache_key,$possibleUPN)) {
							array_push($possibleUPN,$local_cache_key);
							}

				  } //end foreach $local_cache_key

				 foreach (array_unique($possibleUPN) as $mycachekey) {
					     $redis->set($mycachekey, serialize($memberJSON));
                         $redis->expire($mycachekey, Settings::$cache_time_oid);
				 } //end foreach $possibleUPN
            } //end if redis_key_exists
        } //if member has UPN
    } // end foreach members
} // end function graph_CacheGuestsFromArray

function graph_getAllUsers($useCache = true)
{
    if(!isset($redis)) { $redis = new Redis(); }
    $redis->connect(Settings::$redis_host, Settings::$redis_port);
    //$cache_key = md5($sql);
    $cache_key = "ms_graph_api_userlist";
    if ($redis->exists($cache_key) && $useCache == true) {
        ///  // $data_source = "Data from Redis Server";
        $users = [];
        $users = unserialize($redis->get($cache_key));
        return $users;
    } else {
        $token = AuthHelper::getDaemonToken();
        $targetURL = Settings::$unifiedAPIEndpoint . 'users?$select=id,jobTitle,surname,givenName,userPrincipalName,mail,displayName&$top=999';
        $request = curl_init($targetURL);
        curl_setopt($request, CURLOPT_HTTPHEADER, [
            "Authorization: Bearer " . $token->accessToken,
            "Accept: application/json",
        ]);
        curl_setopt($request, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($request);
        //echo '<script>console.log(\' RES: '.$response.'\');</script>';
        //parse the json into oci_fetch_object
        $users = json_decode($response, true);
        if (
            isset($users) &&
            !empty($users) &&
            !isset($users["error"]["code"])
        ) {
            //echo '<code>'
            //print json_encode($response,	JSON_PRETTY_PRINT)."\n";
            //echo '</code>';
            $apiRoot = $users["@odata.context"];
            //$apiRoot = substr($apiRoot, 0, strrpos($apiRoot, "/"));
            //$_SESSION[Settings::$apiRoot] = $apiRoot;
            $redis->set($cache_key, serialize($users));
            $redis->expire($cache_key, Settings::$cache_time_groups);
            graph_CacheUsersFromArray($users);
            return $users;
            // file_put_contents("/tmp/ADgroups.json.tmp",$response);
        } else {
            $retme = [];
            return $retme;
        }
    }
}

//----------CREATE /INVITE ---
function graph_CreateUser($displayName, $mailNickname, $principal, $passwd,$fname=false,$lname=false,$jobTitle=false)
{
    $graphTemplate_CreateUser = [
        "accountEnabled" => true,
        "displayName" => "Template User",
        "mailNickname" => "template-user",
        "userPrincipalName" => "TemplateUser@contoso.onmicrosoft.com",
        "passwordProfile" => [
            "forceChangePasswordNextSignIn" => true,
            "password" => uniqid("DefaultAutoGenDefPass"),
        ],
    ];
    $userRequest = $graphTemplate_CreateUser;
    $userRequest["passwordProfile"]["password"] = $passwd;
    $userRequest["userPrincipalName"] = $principal;
    $userRequest["displayName"] = $displayName;
    $userRequest["mailNickname"] = $mailNickname;
    if($fname!=false)    {
		$userRequest["givenName"] = $fname;
		}
    if($lname!=false)    {
		$userRequest["surname"] = $lname;
		}
    if($jobTitle!=false)    {
		$userRequest["jobTitle"] = $jobTitle;
		}
    $postdata = $userRequest;
    $payload = json_encode($postdata);
    $token = AuthHelper::getDaemonToken();
    $targetURL = Settings::$unifiedAPIEndpoint . "users";
    $request = curl_init($targetURL);
    curl_setopt($request, CURLOPT_HTTPHEADER, [
        "Authorization: Bearer " . $token->accessToken,
        "Accept: application/json",
        "Content-Type:application/json",
    ]);
    curl_setopt($request, CURLOPT_POST, true);
    curl_setopt($request, CURLOPT_POSTFIELDS, $payload);
    curl_setopt($request, CURLOPT_RETURNTRANSFER, true);
    $response = curl_exec($request);
    //echo '<script>console.log(\' RES: '.$response.'\');</script>';
    //parse the json into oci_fetch_object
    //$result = json_decode($response, true);
    //return $result;
    return json_decode($response, true);
}

function graph_InviteUser(
    $useCache = true,
    $displayName,
    $mail,
    $redirURL,
    $userType = "Guest",
    $invitemessage = "InviteMSGNotSet",
    $language = "en-EN"
) {
    //generate template
    $graphTemplate_InviteUser = [
        "accountEnabled" => true,
        "displayName" => "Template User",
        "EmailAddress" => "defaultuser@contoso.onmicrosoft.com",
        "inviteRedirectUrl" => "https://contoso.onmicrosoft.com",
        "invitedUserType" => "Guest",
//        "status" => "Completed",
        "sendInvitationMessage" => true,
        "invitedUserMessageInfo" => [
            "@odata.type" => "microsoft.graph.invitedUserMessageInfo",
            "messageLanguage" => "en-EN",
            //MS is so bad, subject will be empty like this// "customizedMessageSubject" => 'Subject: Invitation to '.Settings::$tenantDOMAIN,
            "customizedMessageBody" =>
                "You to have been invited to create your account on " .
                Settings::$tenantDOMAIN,
        ],
    ];

    if(!isset($redis)) { $redis = new Redis(); }
    $redis->connect(Settings::$redis_host, Settings::$redis_port);
    //$cache_key = md5($sql);
    $cache_key =
        "ms_graph_api_userinvite_" .
        md5($displayName . $mail . $redirURL . $userType);
    if ($redis->exists($cache_key) && $useCache == true) {
        ///  // $data_source = "Data from Redis Server";
        //$invite = array();
        //$invite = unserialize($redis->get($cache_key));
        //return json_decode($invite, true);
        return unserialize($redis->get($cache_key));
    } else {
        $inviteRequest = $graphTemplate_InviteUser;
        $inviteRequest["invitedUserDisplayName"] = $displayName;
        $inviteRequest["inviteRedirectUrl"] = $redirURL;
        $inviteRequest["invitedUserEmailAddress"] = $mail;
        if ($invitemessage != "InviteMSGNotSet") {
            //echo "prepping msg";
            $inviteRequest["invitedUserMessageInfo"][
                "messageLanguage"
            ] = $language;
            $inviteRequest["invitedUserMessageInfo"][
                "customizedMessageBody"
            ] = $invitemessage;
        }

        $postdata = $inviteRequest;

        echo "++sending++";
        var_dump($postdata);
        // echo "##";
        $payload = json_encode($postdata);
        $token = AuthHelper::getDaemonToken();
        $targetURL = Settings::$unifiedAPIEndpoint . "invitations";
        $request = curl_init($targetURL);
        curl_setopt($request, CURLOPT_HTTPHEADER, [
            "Authorization: Bearer " . $token->accessToken,
            "Accept: application/json",
            "Content-Type:application/json",
        ]);
        curl_setopt($request, CURLOPT_POST, true);
        curl_setopt($request, CURLOPT_POSTFIELDS, $payload);
        curl_setopt($request, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($request);
        //echo '<script>console.log(\' RES: '.$response.'\');</script>';
        //parse the json into oci_fetch_object
        //$result = json_decode($response, true);
        //return $result;
        if (!isset($response["error"])) {
			//OK
            $redis->set($cache_key, serialize($response));
            $redis->expire($cache_key, Settings::$cache_time_invites);
        graph_CacheUsersFromArray(graph_getAllUsers(false));
        } else  {
		}
        // new oid needs to be read

        return $response;
    }
}

function graph_InviteUserSendCustomMail(
    $useCache = true,
    $displayName,
    $mail,
    $redirURL,
    $userType = "Guest",
    $inviteSubject = "InviteMSGNotSet",
    $inviteMessage = "InviteMSGNotSet",
    $inviteTail = "InviteMSGNotSet",
    $myreply_to = "UNSET",
    $mail_method = "phpmail",
    $language = "en-EN"
) {
    $graphTemplate_InviteUser = [
        "accountEnabled" => true,
        "displayName" => "Template User",
        "EmailAddress" => "defaultuser@contoso.onmicrosoft.com",
        "inviteRedirectUrl" => "https://contoso.onmicrosoft.com",
        "invitedUserType" => "Guest",
        "status" => "Completed",
        "sendInvitationMessage" => false,
        "invitedUserMessageInfo" => [
            "@odata.type" => "microsoft.graph.invitedUserMessageInfo",
            "messageLanguage" => "en-EN",
            //MS is so bad, subject will be empty like this// "customizedMessageSubject" => 'Subject: Invitation to '.Settings::$tenantDOMAIN,
            "customizedMessageBody" =>
                "You to have been invited to create your account on " .
                Settings::$tenantDOMAIN,
        ],
    ];
     if (Settings::$debugApp||Settings::$debugAppVerbose) {
         echo "++GOT invite request++";
         var_dump(array( "type" => $userType , "subjct" => $inviteSubject ,  "bodyhead" => base64_encode($inviteMessage ) , "bodytail" =>  base64_encode($inviteTail ),"replyTo"=> $myreply_to ));
         echo "##";
     }
    // build a default message
    // subject
    if (empty($inviteSubject) || $inviteSubject != "InviteMSGNotSet") {
        $subject = $inviteSubject;
    } else {
        $subject = "Invitation to " . Settings::$tenantDOMAIN;
    }
    // compose message
    if (empty($inviteMessage) || $inviteMessage != "InviteMSGNotSet") {
        $firstpart = $inviteMessage;
    } else {
        $firstpart =
            "Hello dear  " .
            $respOBJ["invitedUserDisplayName"] .
            " ,<br><br> This is your invitation to join " .
            Settings::$tenantDOMAIN .
            "<br><br> This step is only necesarry once so you mailing-list Mails can be delivered to you. <br> Please click the following Link: <br> ";
    }
    if (empty($inviteTail) || $inviteTail != "InviteMSGNotSet") {
        $secondpart = $inviteTail;
    } else {
        $secondpart=' <br> to confirm your new user account';
    }

    // start invite
    if(!isset($redis)) { $redis = new Redis(); }
    $redis->connect(Settings::$redis_host, Settings::$redis_port);
    //$cache_key = md5($sql);
    $cache_key =
        "ms_graph_api_userinvite_" .
        md5($displayName . $mail . $userType);

    if (Settings::$debugApp||Settings::$debugAppVerbose) {
        var_dump([
            "cached_found" => $redis->exists($cache_key),
            "useCache" => $useCache,
        ]);
    }
    if ($redis->exists($cache_key) && $useCache == true) {
        if (Settings::$debugAppVerbose) {
            echo "Data from Redis Server";
        }
        $invite = [];
        $invite = json_decode(unserialize($redis->get($cache_key)),true);
        return ["status" => "OK","cached"=>true, "result" => $invite ];
        //return json_decode($invite);
        //return $invite;
    } else {
        if (Settings::$debugAppVerbose) {
            echo "data NOT from Redis Server";
        }

        $inviteRequest = $graphTemplate_InviteUser;
        $inviteRequest["invitedUserDisplayName"] = $displayName;
        $inviteRequest["invitedUserType"] = $userType;
        $inviteRequest["inviteRedirectUrl"] = $redirURL;
        $inviteRequest["invitedUserEmailAddress"] = $mail;
        if ($inviteMessage != "InviteMSGNotSet") {
            //echo "prepping msg";
            $inviteRequest["invitedUserMessageInfo"][
                "messageLanguage"
            ] = $language;
            $inviteRequest["invitedUserMessageInfo"][
                "customizedMessageBody"
            ] = $inviteMessage;
        }

        $postdata = $inviteRequest;
        if (Settings::$debugApp||Settings::$debugAppVerbose) {
            print "++sending invite++ \n";
            echo '<code>';
            print_r(htmlentities(json_encode($postdata,JSON_PRETTY_PRINT), ENT_QUOTES));
            echo '</code>';
            echo "## \n";
        }
        $payload = json_encode($postdata);
        $token = AuthHelper::getDaemonToken();
        $targetURL = Settings::$unifiedAPIEndpoint . "invitations";
        $request = curl_init($targetURL);
        curl_setopt($request, CURLOPT_HTTPHEADER, [
            "Authorization: Bearer " . $token->accessToken,
            "Accept: application/json",
            "Content-Type:application/json",
        ]);
        curl_setopt($request, CURLOPT_POST, true);
        curl_setopt($request, CURLOPT_POSTFIELDS, $payload);
        curl_setopt($request, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($request);

        //echo "AFTERCURL";
        //echo '<script>console.log(\' RES: '.$response.'\');</script>';
        //parse the json into oci_fetch_object
        //$result = json_decode($response, true);
        //return $result;

        if (Settings::$debugApp||Settings::$debugAppVerbose) {
            print "\n INVITE RESP: \n";
            print_r(htmlentities(json_encode(json_decode($response),JSON_PRETTY_PRINT), ENT_QUOTES));
        }
        $respOBJ = json_decode($response, true);

        //echo "RESPJSONDEC";var_dump($respOBJ);
        $myfrom =
            strtolower(Settings::$tenantINVITEmail) .
            "@" .
            strtolower(Settings::$tenantDOMAIN);
        if ($myreply_to == "UNSET") {
            $myreply_to =
                strtolower(Settings::$tenantADMINmail) .
                "@" .
                strtolower(Settings::$tenantDOMAIN);
        }
        //only send mail if  graph gives us the Redeem URL
        if (isset($respOBJ["inviteRedeemUrl"]) && !empty($respOBJ["inviteRedeemUrl"])) {
			if ( Settings::$debugAppVerbose|| Settings::$debugAppMail ) {
                print "\nFOUND REDEEMCODE ".$respOBJ["inviteRedeemUrl"]." \n";
                //var_dump([$to, $subject, $message, $headers]);
            }
            $to = $mail;
            if( isset( Settings::$redirect_all_mail ) && Settings::$redirect_all_mail == true && isset( Settings::$redirect_all_mail_to ) ) {
                 $to=md5($mail)."_".Settings::$redirect_all_mail_to;
            }
 			if ( Settings::$debugAppVerbose|| Settings::$debugAppMail ) {
                print "\n MAIL TO".$to." .. ";
            }

            if ($mail_method == "phpmail") {
                if ( Settings::$debugAppVerbose|| Settings::$debugAppMail ) {
                    print "-PHPMAIL";
                }

                // multiple recipients (note the commas)
                //$to = "somebody@example.com, ";
                //$to .= "nobody@example.com, ";
                //$to .= "somebody_else@example.com";

                // compose headers
                $headers = "From: '.$myfrom.'\r\n";
                $headers .= "Reply-To: '" . $myreply_to . "'\r\n";
                $headers .= "X-Mailer: PHP/" . phpversion();
                // To send HTML mail, the Content-type header must be set
                $headers = "MIME-Version: 1.0\r\n";
                $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
            } //end phpmail prep stage

            $message =
                '<html><head><title> ' .
                $subject .
                '</title></head><body>
                <h1> ' . $subject .' </h1><br><p>' .
                $firstpart .
                ' <a href="' .$respOBJ["inviteRedeemUrl"] .'"> ' .$respOBJ["inviteRedeemUrl"] ." </a> " .
                $secondpart .
                ' </p>
              </body>
            </html>';
               if (Settings::$debugAppVerbose   || Settings::$debugApp|| Settings::$debugAppMail ) {

                    //echo "\nprepard sending  VIA " . $mail_method . "  MSG= ".print_r(json_encode('<!-- '.$message.' -->'),JSON_PRETTY_PRINT )." ... ";
                    echo "\nprepard sending  VIA " . $mail_method . "  MSG_Base64= ".base64_encode($message)." ... ";
                    //var_dump([$to, $subject, $message, $headers]);
                }
            // send email
            if ($mail_method == "phpmail") {
                if (Settings::$debugAppVerbose   || Settings::$debugApp|| Settings::$debugAppMail ) {
                    print "\nsending VIA " . $mail_method . " ...  \n";
                    print_r(json_encode([$to, $subject, base64_encode($message)	, $headers]),JSON_PRETTY_PRINT);
                }

                //send at this point
                if(!exists(Settings::$DoNotSendMails) || Settings::$DoNotSendMails != true )
                {
                  $mailres = mail($to, $subject, $message, $headers);
                } else { $mailres=true; };


                if (Settings::$debugApp) {
					 print_r(htmlentities(json_encode($mailres,JSON_PRETTY_PRINT), ENT_QUOTES));

                }

                if ($mailres) {
                    /* php mail() returns true */
                    if (Settings::$debugAppVerbose) {
                        echo "\npushing to redis" . $cache_key;
                    }
                    $redis->set($cache_key, serialize($response));
                    $redis->expire($cache_key, Settings::$cache_time_invites);
                    return ["status" => "OK","cached"=>false, "result" => $response ,"mail_result" => $mailres ];

                    //return json_decode($response);
                } else {
                    /* mail() failed */
                    return [
                        "status" => "FAIL",
                        "reason" => 'MAIL-NOT-SENT',
                        "result" => $response,
                    ];
                }
            } // end phpmail send stage

            if ($mail_method == "graph_API") {
				$headers=array();
                $fromuser = graph_getUserByOID(
                    graph_getOID_byMail_OR_Principal($myfrom)
                );
                if (Settings::$debugAppVerbose   || Settings::$debugApp|| Settings::$debugAppMail ) {
                    print "\n prepare sending inivtation VIA " . $mail_method . " FROM OID: ".graph_getOID_byMail_OR_Principal($myfrom). " ... \n";
                    //var_dump([$to, $subject, $message, $headers]);
                }

                $mydisplayname = " Invitation to " . strtolower(Settings::$tenantDOMAIN);
                if (isset($fromuser["displayName"])) {
                    $mydisplayname = $fromuser["displayName"];
                }
                $token = AuthHelper::getDaemonToken();
                $graphMailer = new graphMailer($token->accessToken);
                $mailArgs = [
                    "subject" => $subject,
                    "body" => $message,
                    "replyTo" => [
                        "name" => $mydisplayname, // name is optional
                        "address" => $myreply_to,
                        ],
                    "toRecipients" => [
                        ["name" => $displayName, "address" => $mail],
                        ],
                     "ccRecipients" => [], // name is optional, otherwise array of address=>email@address
                        "importance" => "normal",
                        "conversationId" => "", //optional, use if replying to an existing email to keep them chained properly in outlook
                ];

                if (Settings::$debugAppVerbose   || Settings::$debugApp|| Settings::$debugAppMail ) {
                    print "\n sending inivtation VIA " . $mail_method . " FROM OID: ".graph_getOID_byMail_OR_Principal($myfrom). " ... \n";
                    //var_dump([$to, $subject, $message, $headers]);
                }
                if(!isset(Settings::$DoNotSendMails) || Settings::$DoNotSendMails != true  )
                {
                     $mailres = $graphMailer->sendMail(strtolower(Settings::$tenantINVITEmail) . "@".Settings::$tenantDOMAIN,   $mailArgs );
                } else { $mailres=array("status"=>"OK","reason"=>"NOT_SENDING DUE TO SETTINGS"); };


                if (Settings::$debugAppVerbose   || Settings::$debugApp|| Settings::$debugAppMail ) {
                    print "\n sent request VIA " . $mail_method . " FROM OID: ".graph_getOID_byMail_OR_Principal($myfrom). " ... \n";
                    //var_dump([$to, $subject, $message, $headers]);
                }

                if ($mailres["status"]=="OK") {
                    /* php mail() returns true */
                    if (Settings::$debugAppVerbose) {
                        echo "\n pushing to redis" . $cache_key;
                    }
                    $redis->set($cache_key, serialize($response));
                    $redis->expire($cache_key, Settings::$cache_time_invites);
                    return ["status" => "OK","cached"=>false, "result" => $response ,"mail_result" => $mailres ];
                    //return json_decode($response);
                } else {
		            if (Settings::$debugAppVerbose   || Settings::$debugApp|| Settings::$debugAppMail ) {
                      echo "\n sending FAILED VIA " . $mail_method . " FROM OID: ".graph_getOID_byMail_OR_Principal($myfrom). " ... ";

                      var_dump([$to, $subject, base64_encode($message), $headers]);
                    }

                    /* mail() failed */
                    return array(
                        "status" => "FAIL",
                        "reason" => 'MAIL-NOT-SENT',
                        "result" => $response,
                        "mailresult"=>$mailres["result"],
                    );
                }
            } // end grapHh_API mail stage
        } else {
			//  no redeemcode found;
			if ( Settings::$debugAppVerbose|| Settings::$debugAppMail ) {
                echo "\nNO  REDEEMCODE ";
                //var_dump([$to, $subject, $message, $headers]);
            }
			return array(
                        "status" => "FAIL",
                        "reason"=> 'NO-REDEEM-URL',
                        "result" => $response,
                    );
           /*API FAILED invite since no RedeemURL was found */
        }
    }
}
