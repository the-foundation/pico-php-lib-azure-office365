<?php

require_once("/var/www/apitest-settings.php");
$sendTestmail=true;
$deleteGuestUsers=true;

error_reporting(E_ALL|E_STRICT);
ini_set("display_errors", 1);
ini_set('xdebug.var_display_max_depth', 10);
ini_set('xdebug.var_display_max_children', 256);
ini_set('xdebug.var_display_max_data', 1024);
global $bootmsg;
$bootmsg="";
if(php_sapi_name() != "cli") {
	session_start();
	}


if (!class_exists("Settings"))	 {
  	require_once( dirname(__FILE__) . "/Settings.php");

  }

//Settings::$debugApp=true;
//Settings::$debugAppVerbose=true;
//Settings::$debugAppMail=true;

//require_once("AuthHelper.php");
//require_once("Token.php");
require_once("MS-graph-functions.php");




echo '<html><head></head><body><center>';

echo '<table style="border:2px solid;max-width: 95%;min-width: 55%;" ><tbody>';

///
echo '<tr><td style="border:1px solid;" ><code>';
echo "###counting groups @".strtolower(Settings::$tenantDOMAIN);
echo '</code></td>';echo '<td style="border:1px solid;" ><code>';
echo count(graph_getAllGroups()["value"]);
echo '</code></td>';echo '</tr>';

echo '<tr><td style="border:1px solid;" ><code>';
echo "###counting users @".strtolower(Settings::$tenantDOMAIN);
echo '</code></td>';echo '<td style="border:1px solid;" ><code>';
echo count(graph_getAllUsers()["value"]);
echo '</code></td>';echo '</tr>';
///

echo '<tr><td style="border:1px solid;" ><code>';
echo '###counting GUEST USERS<br> WITH NO GROUP<br> @'.strtolower(Settings::$tenantDOMAIN);
echo '</code></td>';echo '<td style="border:1px solid;" ><code>';
print_r(count(graph_getGuestUsersWithNoGroupMembership()));
echo '</code></td>';echo '</tr>';

if(isset($APITEST_delete_enable) && $APITEST_delete_enable) {
  echo '<tr><td style="border:1px solid;" ><code>';
  echo '###DELETEING GUEST USERS<br> WITH NO GROUP<br> @'.strtolower(Settings::$tenantDOMAIN);
  echo '</code></td>';echo '<td style="border:1px solid;" ><code>';
  var_dump(graph_DeleteGuestUsersWithNoGroupMembership());
  echo '</code></td>';echo '</tr>';

  echo '<tr><td style="border:1px solid;" ><code>';
  echo '###ATFTER DELETIION<br>counting GUEST USERS<br> WITH NO GROUP<br> @'.strtolower(Settings::$tenantDOMAIN);
  echo '</code></td>';echo '<td style="border:1px solid;" ><code>';
  print_r(count(graph_getGuestUsersWithNoGroupMembership()));
  echo '</code></td>';echo '</tr>';

}


echo '<tr><td style="border:1px solid;" ><code>';
echo "###creating GROUP graph_CreateGroup():: name: test-group-api-created@".strtolower(Settings::$tenantDOMAIN);
echo '</code></td>';echo '<td style="border:1px solid;" ><code>';
var_dump(graph_CreateGroup("test-group-api-created"));
echo '</code></td>';echo '</tr>';


echo '<tr><td style="border:1px solid;" ><code>';
echo "###creating USER : apitest-created-user@".strtolower(Settings::$tenantDOMAIN);
echo '</code></td>';echo '<td style="border:1px solid;" ><code>';
var_dump(graph_CreateUser("APITEST CREATEDUSER","apitest-created-user","apitest-created-user@".Settings::$tenantDOMAIN,uniqid("DefaultAutoGenDefPass")));
echo '</code></td>';echo '</tr>';


echo '<tr><td style="border:1px solid;" ><code>';
echo "### ADDing USERs ( tenantADMINmail  USER : apitest-created-user@".strtolower(Settings::$tenantDOMAIN)." to GROUP : allcompany@".strtolower(Settings::$tenantDOMAIN);
echo '</code></td>';echo '<td style="border:1px solid;" ><code>';
$addusers=array();
$addmail=graph_getOID_byMail(strtolower(Settings::$tenantADMINmail)."@".strtolower(Settings::$tenantDOMAIN));
if(!empty($addmail))  { array_push($addusers,$addmail);}
$addmail=graph_getOID_byMail_OR_Principal(strtolower("apitest-created-user@").strtolower(Settings::$tenantDOMAIN));
if(!empty($addmail))  { array_push($addusers,$addmail);}

var_dump( json_encode (
graph_AddUsersToGroup(
  array(
      "AddToGroupOID" => $test_group_oid,
      "userOIDlist" => $addusers

      )
  ) , JSON_PRETTY_PRINT)

);
echo '</code></td>';echo '</tr>';

echo '<tr><td style="border:1px solid;" ><code>';
echo "### DELing  SINGLE USER    : apitest-created-user@".strtolower(Settings::$tenantDOMAIN)." from  GROUP : ".graph_getMailAdress_byOID( $test_group_oid );
echo '</code></td>';echo '<td style="border:1px solid;" ><code>';
var_dump(
graph_DeleteUserFromGroup(
  array(
      "DelFromGroupOID" => $test_group_oid,
      "userOID" => graph_getOID_byMail_OR_Principal(strtolower("apitest-created-user@").strtolower(Settings::$tenantDOMAIN)) ,
      )
  )
);
echo '</code></td>';echo '</tr>';

echo '<tr><td style="border:1px solid;" ><code>';
echo "### DELing MULTI USER   : from GROUP : allcompany@".strtolower(Settings::$tenantDOMAIN);
echo '</code></td>';echo '<td style="border:1px solid;" ><code>';
var_dump(
graph_DeleteUsersFromGroup(
  array(
      "DelFromGroupOID" => $test_group_oid,
      "userOIDlist" => $array,

      )
  )

);
echo '</code></td>';echo '</tr>';
///echo '<tr><td style="border:1px solid;" ><code>';
///echo "###resolving default user(by mail):".strtolower(Settings::$tenantADMINmail)."@".strtolower(Settings::$tenantDOMAIN);
///echo '</code></td>';echo '<td style="border:1px solid;" ><code>';
///var_dump(graph_getOID_byMail(strtolower(Settings::$tenantADMINmail)."@".strtolower(Settings::$tenantDOMAIN)));
///echo '</code></td>';echo '</tr>';
///
///echo '<tr><td style="border:1px solid;" ><code>';
///echo "###resolving default user(by principal)".strtolower(Settings::$tenantADMINmail)."@".strtolower(Settings::$tenantDOMAIN);
///echo '</code></td>';echo '<td style="border:1px solid;" ><code>';
///var_dump(graph_getOID_byMail_OR_Principal(strtolower(Settings::$tenantADMINmail)."@".strtolower(Settings::$tenantDOMAIN)));
///echo '</code></td>';echo '</tr>';
///echo '<tr><td style="border:1px solid;" ><code>';
///echo "###resolving default user mail address (by oid) ".strtolower(Settings::$tenantADMINmail)."@".strtolower(Settings::$tenantDOMAIN);
///echo '</code></td>';echo '<td style="border:1px solid;" ><code>';
///var_dump(graph_getMailAdress_byOID("2a228425-a76f-42ce-b30d-6b247a8a1382"));
///echo '</code></td>';echo '</tr>';
///echo '<tr><td style="border:1px solid;" ><code>';
///echo "###resolving default user principal(by oid) ".strtolower(Settings::$tenantADMINmail)."@".strtolower(Settings::$tenantDOMAIN);
///echo '</code></td>';echo '<td style="border:1px solid;" ><code>';
///var_dump(graph_getMailAdress_byOID("2a228425-a76f-42ce-b30d-6b247a8a1382"));
///echo '</code></td>';echo '</tr>';
///
///echo '<tr><td style="border:1px solid;" ><code>';
///echo "###resolving (recursive) default user principal(by oid from mail_or_principal ) ".strtolower(Settings::$tenantINVITEmail)."@".strtolower(Settings::$tenantDOMAIN);
///echo '</code></td>';echo '<td style="border:1px solid;" ><code>';
///$oid=graph_getOID_byMail_OR_Principal(strtolower(Settings::$tenantINVITEmail)."@".strtolower(Settings::$tenantDOMAIN));
///var_dump($oid);
///var_dump(graph_getUserByOID($oid));
///echo '</code></td>';echo '</tr>';
///


//$users=graph_Users();
////var_dump($users);
//graph_CacheUsersFromArray($users,true);
$newname=$testinvitename_TWO;
$invitedmail=$testinviteemail_ONE;
echo '<td style="border:1px solid;" ><code>';
echo "###INVITE USER WITH phpmail BY graph_InviteUserSendCustomMail == ".strtolower($newname)." TO ".strtolower($invitedmail);
echo '</code></td>';echo '<td style="border:1px solid;" ><code>';
$invited=graph_InviteUserSendCustomMail(true,
                                        $newname,
                                        $invitedmail,
                                        Settings::$inviteRedirURL,
                                        "Guest",
                                        "Einladung zu ".strtolower(Settings::$tenantDOMAIN),
                                        'Sehr geehrte_r '.$newname.'<br><br> Dies ist eine Einladung zu '.strtolower(Settings::$tenantDOMAIN).' , bitte best&auml;tigen Sie ihren Account <br>  um Nachrichten von Mailinglisten auf '.strtolower(Settings::$tenantDOMAIN).' zu erhalten,<br>indem sie den folgenden Link anklicken:<br> ',
                                        '<br>Vielen Dank<br>',
                                        strtolower(Settings::$tenantINVITEmail)."@".strtolower(Settings::$tenantDOMAIN),"phpmail");

//var_dump(json_decode($invited));
var_dump($invited);
echo '</code></td>';echo '</tr><tr>';

$newname="External TesterB";

$invitedmail=$testinviteemail_TWO;
echo '<td style="border:1px solid;" ><code>';
echo "###INVITE USER (uncached) WITH graph_API BY graph_InviteUserSendCustomMail == ".strtolower($newname)." TO ".strtolower($invitedmail);
echo '</code></td>';echo '<td style="border:1px solid;" ><code>';
$invited=graph_InviteUserSendCustomMail(false,
                                        $newname,
                                        $invitedmail,
                                        Settings::$inviteRedirURL,
                                        "Guest",
                                        "Einladung zu ".strtolower(Settings::$tenantDOMAIN),
                                        'Sehr geehrte_r '.$newname.'<br><br> Dies ist eine Einladung zu '.strtolower(Settings::$tenantDOMAIN).' , bitte best&auml;tigen Sie ihren Account <br>  um Nachrichten von Mailinglisten auf '.strtolower(Settings::$tenantDOMAIN).' zu erhalten,<br>indem sie den folgenden Link anklicken:<br> ',
                                        '<br>Vielen Dank<br>',
                                        strtolower(Settings::$tenantINVITEmail)."@".strtolower(Settings::$tenantDOMAIN),"graph_API");

//var_dump(json_decode($invited));
var_dump($invited);
echo '</code></td>';echo '</tr><tr>';


echo '<td style="border:1px solid;" ><code>';
echo "###INVITE USER (  cached) WITH graph_API BY graph_InviteUserSendCustomMail == ".strtolower($newname)." TO ".strtolower($invitedmail);
echo '</code></td>';echo '<td style="border:1px solid;" ><code>';
$invited=graph_InviteUserSendCustomMail(true,
                                        $newname,
                                        $invitedmail,
                                        Settings::$inviteRedirURL,
                                        "Guest",
                                        "Einladung zu ".strtolower(Settings::$tenantDOMAIN),
                                        'Sehr geehrte_r '.$newname.'<br><br> Dies ist eine Einladung zu '.strtolower(Settings::$tenantDOMAIN).' , bitte best&auml;tigen Sie ihren Account <br>  um Nachrichten von Mailinglisten auf '.strtolower(Settings::$tenantDOMAIN).' zu erhalten,<br>indem sie den folgenden Link anklicken:<br> ',
                                        '<br>Vielen Dank<br>',
                                        strtolower(Settings::$tenantINVITEmail)."@".strtolower(Settings::$tenantDOMAIN),"graph_API");

//var_dump(json_decode($invited));
var_dump($invited);
echo '</code></td>';echo '</tr><tr>';



$newname="External TesterD-ContosoInvite";
$invitedmail=$testinviteemail_THREE;

echo '<td style="border:1px solid;" ><code>';
echo "###INVITE USER WITH graph_InviteUser == ".strtolower($newname)." TO ".strtolower($invitedmail);
echo '</code></td>';echo '<td style="border:1px solid;" ><code>';
$invited=graph_InviteUser(true,
                                        $newname,
                                        $invitedmail,
                                        Settings::$inviteRedirURL,
                                        "Guest",
                                        "Einladung zu ".strtolower(Settings::$tenantDOMAIN),
                                        'Sehr geehrte_r '.$newname.'<br><br> Dies ist eine Einladung zu '.strtolower(Settings::$tenantDOMAIN).' , bitte best&auml;tigen Sie ihren Account <br>  um Nachrichten von Mailinglisten auf '.strtolower(Settings::$tenantDOMAIN).' zu erhalten,<br>indem sie den folgenden Link anklicken:<br> <br><hr><br>Mit freundlichen Gr&uuml;&szlig;en , ihre _ORGNAME_<br>',strtolower(Settings::$tenantINVITEmail)."@".strtolower(Settings::$tenantDOMAIN));


var_dump(json_decode($invited));
//var_dump($invited);
echo '</code></td>';echo '</tr><tr>';




//////////###
////echo '<td style="border:1px solid;" ><code>';
////echo "###SEND MAIL by graphMailer == ".strtolower($newname)." TO ".strtolower($invitedmail);
////echo '</code></td>';echo '<td style="border:1px solid;" ><code>';
////$token = AuthHelper::getDaemonToken();
////$graphMailer = new graphMailer($token->accessToken);
////$mailArgs =  array('subject' => 'Einladung - Bitte Account bestaetigen .. TESTING 	MS Graph API Mailing is a charm',
////    'replyTo' => array('name' => 'Reply ToMe', 'address' => strtolower(Settings::$tenantADMINmail)."@".strtolower(Settings::$tenantDOMAIN)),
////    'toRecipients' => array( // can have multiple
////        array('name' => $testinvitename_ONE, 'address' => $testinviteemail_THREE)
////    ),     // name is optional
////    'ccRecipients' => array(
////        array('name' => $testinvitename_ONE , 'address' => $testinviteemail_ONE ),
////        array('name' => $testinvitename_TWO, 'address' => $testinviteemail_TWO)
////     ),     // name is optional, otherwise array of address=>email@address
////    'importance' => 'normal',
////    'conversationId' => '',   //optional, use if replying to an existing email to keep them chained properly in outlook
////    'body' => 'Sehr geehrte_r  __ <br><br> Dies ist eine Einladung zu '.strtolower(Settings::$tenantDOMAIN).' ,
////                 bitte best&auml;tigen Sie ihren Account <br>  um Nachrichten von Mailinglisten auf '.strtolower(Settings::$tenantDOMAIN).' zu erhalten,  <br>
////                 indem sie den folgenden Link anklicken:<br> TEST_ONLY_AFTER_SIGNUP_URL: '.Settings::$inviteRedirURL.' <br><hr><br>Mit freundlichen Gr&uuml;&szlig;en , ihre ORGNAME<br>',
////  //  'images' => array(
////  //      array('Name' => 'assets/logo.png', 'ContentType' => 'image/png', 'Content' => file_get_contents("assets/logo.png"), 'ContentID' => 'cid:blah')
////  //  ),   //array of arrays so you can have multiple images. These are inline images. Everything else in attachments.
////  //  'attachments' => array(
////  //      array('Name' => 'assets/bg.png', 'ContentType' => 'image/png', 'Content' => file_get_contents("assets/bg.png"))
////  //  )
////  );
////
////if ($sendTestmail) {
////  print_r(addslashes(json_encode($graphMailer->sendMail(strtolower(Settings::$tenantADMINmail)."@".strtolower(Settings::$tenantDOMAIN), $mailArgs),JSON_PRETTY_PRINT)));
////}
////echo '</code></td>';echo '</tr><tr>';



//----------CREATE /INVITE ---
function graph_sendMail(array $myconfig) {
//////////
//////////$graphTemplate_sendMail = array(
//////////
//////////   "message" => array(
//////////  //      "replyTo" => array("emailAddress" => array()),
//////////
//////////     "toRecipients" => array(
//////////         "emailAddress" => array(
//////////           "address" => strtolower(Settings::$tenantADMINmail)."@".strtolower(Settings::$tenantDOMAIN)
//////////         )
//////////       ),
//////////    //"internetMessageHeaders" => array(),
//////////    "subject" => "DefaultSubject",
//////////    "body" =>  array(
//////////      "contentType" =>  "Text",
//////////      "content" =>  "DefaultTextMSG",
//////////
//////////
//////////    ),
//////////     "saveToSentItems" => false
////////// )
////////// );
//////////
//////////
/////////////    "ccRecipients": [
/////////////      {
/////////////        "emailAddress": {
/////////////          "address": strtolower(Settings::$tenantADMINmail)."@".strtolower(Settings::$tenantDOMAIN)
/////////////        }
/////////////      }
//////////
////////////if(isset($myconfig["ccRecipients"]))     {
////////////$graphTemplate_sendMail["message"]["ccRecipients"] = array("emailAddress" => array());
////////////	if (is_array($myconfig["ccRecipients"])) {
////////////	foreach($myconfig["ccRecipients"] AS $ccUser) {
////////////     array_push($graphTemplate_sendMail["message"]["ccRecipients"]["emailAddress"],array( "address" => $ccUser));
////////////      }
////////////    } else if (is_string($myconfig["ccRecipients"])) {
////////////    // string
////////////    $graphTemplate_sendMail["ccRecipients"]["emailAddress"]["address"]=$myconfig["ccRecipients"];
////////////  } ; }
//////////if(isset($myconfig["to"]))                 {
//////////  if (is_array($myconfig["to"])) {
//////////	foreach( $myconfig["to"] AS $ccUser ) {
//////////     array_push($graphTemplate_sendMail["message"]["toRecipients"]["emailAddress"],array( "address" => $ccUser));
//////////    }
//////////  } else if (is_string($myconfig["to"])) {
//////////    // string
//////////     $graphTemplate_sendMail["message"]["toRecipients"]["emailAddress"]["address"]=$myconfig["to"];   }
////////// }
//////////
////////////if(isset($myconfig["from"]))               {
////////////  $graphTemplate_sendMail["message"]["from"] = array("emailAddress" => array());
////////////	$graphTemplate_sendMail["message"]["from"]["emailAddress"]["address"]=$myconfig["from"]; }
//////////if(isset($myconfig["headers"]))            {
//////////	foreach($myconfig["headers"] AS $header => $val) {
//////////     array_push($graphTemplate_sendMail["message"]["internetMessageHeaders"],array( "name" => $header, "value" => $val ));
//////////    }
//////////}
//////////if(isset($myconfig["subject"]))               { $graphTemplate_sendMail["message"]["subject"]=$myconfig["subject"] ; }
//////////
//////////if(isset($myconfig["body"]))               { $graphTemplate_sendMail["message"]["body"]["content"]=$myconfig["body"] ; }
//////////if(isset($myconfig["contentType"]))        { $graphTemplate_sendMail["message"]["body"]["contentType"]=$myconfig["contentType"] ; }
//////////if(isset($myconfig["save"]))               { $graphTemplate_sendMail["message"]["saveToSentItems"]=$myconfig["save"] ; }
////////////if(isset($myconfig["replyTo"]))            { $graphTemplate_sendMail["message"]["replyTo"]["emailAddress"]["address"]=$myconfig["replyTo"] ;
////////////	                              array_push($graphTemplate_sendMail["message"]["internetMessageHeaders"],array( "name" => "Reply-To", "value" => $myconfig["replyTo"] )); ; }
//////////

///$from=strtolower(Settings::$tenantADMINmail)."@".strtolower(Settings::$tenantDOMAIN);
///if(isset($myconfig["from"]) && strstr( $myconfig["from"], '@' )) {
///  $from=$myconfig["from"];
///}
///$html = "<p>The <b>quick</b> <em>brown</em> <u>fox</u> jumped right over the lazy dog.</p><hr />";
///
///$subject  = "This is a MIME encoded email";
///$boundary = str_replace(" ", "", date('l jS \of F Y h i s A'));
///$newline  = "\r\n";
///
///$headers = "From: ".$from.$newline."To: ".$myconfig["to"].$newline;
///if(isset($myconfig["ccRecipients"]))     {
///           $headers=$headers."Cc: ".implode(", ",$myconfig["ccRecipients"]).$newline; }
///if(isset($myconfig["bccRecipients"]))     {
///                      $headers=$headers."Bcc: ".implode(", ",$myconfig["bccRecipients"]).$newline; }
///if(isset($myconfig["replyTo"]))   {
///  $headers=$headers."Reply-To: ".$myconfig["replyTo"].$newline; }
///
///$headers=$headers."MIME-Version: 1.0$newline".
///           "Content-Type: multipart/alternative;".
///           "boundary = \"$boundary\"$newline$newline".
///           "--$boundary$newline".
///           "Content-Type: text/html; charset=ISO-8859-1$newline".
///           "Content-Transfer-Encoding: base64$newline$newline";
///
///$headers = $headers.rtrim(chunk_split(base64_encode($myconfig["body"])));
///
//mail($to,$subject,"",$headers);

//$messageRequest = $graphTemplate_sendMail;
//$postdata=$messageRequest;
	  //var_dump($messageRequest);
	  //var_dump(json_encode($messageRequest));
	  //$payload=json_encode($postdata);
//var_dump($headers);
//    $payload=$headers;
//	  $token = AuthHelper::getDaemonToken();
//      $targetURL=Settings::$unifiedAPIEndpoint . "users/".$myconfig["principal_OR_mail"]."/sendMail";
//
//  var_dump($targetURL);
//      $request = curl_init($targetURL);
//      curl_setopt($request, CURLOPT_HTTPHEADER, array(
//        "Authorization: Bearer " . $token->accessToken,
//      //  "Accept: application/json",
//      //  "Content-Type:application/json",
//      //  "Content-Type: text/plain"
//      ));
//      curl_setopt($request,CURLOPT_POST, true);
//      curl_setopt($request,CURLOPT_POSTFIELDS, $payload);
//      curl_setopt($request, CURLOPT_RETURNTRANSFER, true);
//      $response = curl_exec($request);
//      //echo '<script>console.log(\' RES: '.$response.'\');</script>';
//      //parse the json into oci_fetch_object
//      //$result = json_decode($response, true);
//      //return $result;
//    var_dump($response);
//      return json_decode($response, true);
     }


//var_dump(graph_sendMail(array(
//		"subject" => "Bestätigen sie Ihre Einladung",
//    "principal_OR_mail" => strtolower(Settings::$tenantADMINmail)."@".strtolower(Settings::$tenantDOMAIN),
//    "to" => "defaultrecipient@dogmigrate20.anonaddy.me",
//   "contentType" => "HTML",
//   "body" => 'Sehr geehrte_r  __ <br><br> Dies ist eine Einladung zu '.strtolower(Settings::$tenantDOMAIN).' , bitte best&auml;tigen Sie ihren Account <br>  um Nachrichten von Mailinglisten auf '.strtolower(Settings::$tenantDOMAIN).' zu erhalten,<br>indem sie den folgenden Link anklicken:<br> <br><hr><br>Mit freundlichen Gr&uuml;&szlig;en , ihre DOG<br>',
//   "save" => false,
//   "ccRecipients" => array("testuserb@msdogmigrate.anonaddy.me","testusera@msdogmigrate.anonaddy.me","msdogtestuser1@sharklasers.com"),
//   "replyTo" => strtolower(Settings::$tenantADMINmail)."@".strtolower(Settings::$tenantDOMAIN)
//
//   )
//));

///#testuserb@msdogmigrate.anonaddy.me
///#testusera@msdogmigrate.anonaddy.me
///#*@dogmigrate20.anonaddy.me
echo '</tbody></table>';
echo '<hr>';

echo '<hr>';
echo '</center></body></html>';
