Function Stem Collector
===
```
function graph_AddUsersToGroup($groupArgs) {
        $dumpArgs=false;
      //  if (Settings::$debugAppVerbose   || Settings::$debugApp|| Settings::$debugAppMail ) { $dumpArgs=true;  }
        if (Settings::$debugAppVerbose   ) { $dumpArgs=true;  }
        if($dumpArgs) {
            print(json_encode($groupArgs,JSON_PRETTY_PRINT) );
        }
        /*
        {
$jsn='{
    "members@odata.bind": [
        "https://graph.microsoft.com/v1.0/directoryObjects/abc00111-1111-22bb-33cc-666123abc999"
    ]
}';    
        $groupArgs[
                AddToGroupOID,   
                userOIDlist[]
                ]
        */


        if(!isset($groupArgs["AddToGroupOID"])) { return "false" ;}
        if(!isset($groupArgs["userOIDlist"])) { return "false" ;}
$addusers=array();        

//$groupArray=json_decode($jsn,true);
//$obj=json_decode($groupArray);

//var_dump($groupArray);


//$firstOID=true;
//unset($obj["members@odata.bind"][0]);

       foreach ($groupArgs["userOIDlist"] as $userOID) {
//echo "add $userOID";
if (!empty($userOID))
{
array_push($addusers,"https://graph.microsoft.com/v1.0/directoryObjects/".$userOID);
}
                        //$groupArray["members@odata.bind"][] = [ $userOID ];
//if ($firstOID) {
//                        $groupArray["members@odata.bind"][0] =  $userOID ;
//                        $firstOID=false;
//} else {
//                        $groupArray["members@odata.bind"][] =  $userOID ;
//}
//
//}                       

//var_dump($messageJSON);
//$sendjsn=json_encode($groupArray);
//$groupArray=json_decode($sendjsn,true);
//var_dump($groupArray);




            //array_push($groupArray["members@odata.bind"],array($userOID));
            //$messageArray["ccRecipients"][] = [
            //        "emailAddress" => [
            //            "name" => $recipient["name"],
            //            "address" => $recipient["address"],
            //        ]
  } 
  
  $messageJSON='{
    "members@odata.bind": '.json_encode($addusers).'
}';
       
       if($dumpArgs) {
                    var_dump(json_decode($messageJSON))   ;
        // $messageJSON = json_encode($groupArray,JSON_PRETTY_PRINT);
        }

        //$messageJSON = json_encode($groupArray);
       // $payload = $messageJSON;
        $token = AuthHelper::getDaemonToken();
        $targetURL =
            Settings::$unifiedAPIEndpoint . "groups/" . $groupArgs["AddToGroupOID"];

        //	 var_dump($targetURL);
        $request = curl_init($targetURL);
        curl_setopt($request, CURLOPT_HTTPHEADER, [
            "Authorization: Bearer " . $token->accessToken,
            "Accept: application/json",
            "Content-Type:application/json",
            //  "Content-Type: text/plain"
        ]);

       //curl_setopt($request, CURLOPT_POST, true);
        
        //FAILS: curl_setopt($request	, CURLOPT_PATCH, true);
        curl_setopt($request, CURLOPT_CUSTOMREQUEST, 'PATCH');

        if($dumpArgs) {
			echo "send TO ".$targetURL;
            var_dump($messageJSON);
        }

        curl_setopt($request, CURLOPT_POSTFIELDS, $messageJSON);
        curl_setopt($request, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($request);
        //echo '<script>console.log(\' RES: '.$response.'\');</script>';
        //parse the json into oci_fetch_object
        //$result = json_decode($response, true);
        //return $result;
        //var_dump($response);
        return json_decode($response, true);

        ///var_dump(json_decode($response));

        if ($response["code"] == "202") {
            return array ( "status"=>"OK","result"=>$response);
        } else { 
			if(isset ($response["error"])  && isset ($response["error"]["message"]) && strpos($response["error"]["message"],'One or more added object references already exist for the following modified properties') !== false)  {
			  return array ( "status"=>"OK","result"=>$response);

			 } else { 
				 //failed 
				 return array ( "status"=>"FAIL","result"=>$response);
				 }
			
			
			}
        
         
    }
```
