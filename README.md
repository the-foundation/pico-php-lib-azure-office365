# Pico Php Lib Azure Office365



## Getting started

PHP MS GRAPH API LIB


#  Instructions

> Names of menu entries and the Whole Microsoft infra might change , you will see by comparing original [README](README.orig.md) below


## Setup ( updated : 2022/Q1)

## Registering the App

## (quickhint [Dashboard→Contoso→Search AD Catalogue](https://aad.portal.azure.com/#blade/Microsoft_AAD_IAM/ActiveDirectoryMenuBlade/Overview)
The first step in developing an application that connects to Office 365 is registering an application with Azure AD.

1. ## Sign-in to the [Azure Management Portal](https://manage.windowsazure.com "Azure Management Portal") using an account that has administrator access to the Azure AD Directory for Office 365.
2. ## [Locate and select the ←← **Active Directory** option](https://aad.portal.azure.com/#blade/Microsoft_AAD_IAM/UsersManagementMenuBlade/MsGraphUsers) towards the bottom of the left navigation (if you don't have a full Azure subscription, it might be your only option).
3. ## In the directory listing, select the directory associated with the Office 365 subscription you want to work with,
4. ## Next, select the **Company Applications** tab in the top navigation:
5. ## Click on the **NEW APP** button at the top  add application catalogue:
6. ## Select **"Create own Application"**
7. ## On the Popup to the left  **"Register an application to integrate with Azure AD (App you're developing)"**
7. ## On the next screen, provide a **Name** for the application, keep the **Type** set to **Web Application and/or Web API** and click the next arrow.
8. ## Next, provide a **Sign-On URL** (points to where you want tokens returned https://somewhere.in.the.inter.net.lan/groupsync/login.php) and **App ID URI** (any globally unique URI such as https://tenant.onmicrosoft.com/MyPHPGroupsApp). Don't worry, these values can be changed later:

![Setting it UP](MSSETUP.jpg)

9. ## Confiure the Basics of the app , ( grant access to the users that should be able to see the gui )
10. ##  either grant the office 365 api access manually or launch integration assistant( seletct "uses APIs" + webapp + daemon )
   * API Access needs permissions as shown in pictures ( at least user@R and group@R/W )
   * create ttoken/secret/password under "the certs and secrets" entry to the left
   * create App-ID-URI under "make app available"

![App Permissions](permissions.jpg)

11. ## **do whatever your custom app needs**
   example softlink structure:( webroot in /var/www/html/, libs in /var/www)

```
www-data@sync:~/html$ ls -lh1
total 16K
lrwxrwxrwx 1 www-data www-data   53 Mar  9 10:43 AfterSignup.php -> /var/www/pico-php-lib-azure-office365/AfterSignup.php
drwxr-xr-x 2 www-data www-data 4.0K Mar  9 08:49 assets
-rw-r--r-- 1 www-data www-data 3.4K Mar  8 02:23 Menu.php
lrwxrwxrwx 1 www-data www-data   22 Mar  9 09:31 MS-graph-functions.php -> MS_graph_functions.php
lrwxrwxrwx 1 www-data www-data   60 Mar  9 09:57 MS_graph_functions.php -> /var/www/pico-php-lib-azure-office365/MS_graph_functions.php
lrwxrwxrwx 1 www-data www-data   44 Mar  9 09:57 pico_lib_functions.php -> /var/www/pico-php-lib/pico_lib_functions.php
lrwxrwxrwx 1 www-data www-data   12 Mar  9 09:28 settings.php -> Settings.php
lrwxrwxrwx 1 www-data www-data   15 Mar  9 09:28 Settings.php -> ../Settings.php
lrwxrwxrwx 1 www-data www-data   41 Mar  9 10:55 css -> /var/www/pico-php-lib-azure-office365/css
lrwxrwxrwx 1 www-data www-data   45 Mar  9 10:55 scripts -> /var/www/pico-php-lib-azure-office365/scripts

```

12. ## be sure to lock down the webserver to not expose `.env` files

13. ## Updating Settings.php ##
The solution contains a Settings.php file, which contains all the settings specific to the application. It needs to be updated with many of the values captured from the application registration process we just finished in Azure Active Directory. Specifically, values for $clientId, $password, and $redirectURI should be updated to reflect the values from your application registration in Azure AD.

1. $clientId should be set to the value from **Step 11** above
2. $password should be set to the value from **Step 13** above
3. $redirectURI should be set to the **Sign-on URL** value from **Step 8** above

Here is an example Settings.php file:

	<?php
class Settings    {

		public static $debugApp = false	 ;
	    public static $debugAppMail = false	 ;
        public static $debugAppVerbose = false;




        public static $styleSheetPHPfile="/var/www/html/style.php";
        //our intermediate site after a user signed up
        public static $inviteRedirURL = 'https://o365sync.orgorg.org/AfterSignup.php';
        //our final URL where the user is finally redirected to after signup
        public static $inviteRedirURLTarget = 'https://myorg.orgorg.org';

        public static $logdir = '/var/www/synclogs';
        public static $redis_port = '6379';
        public static $redis_host = '127.0.0.1';
        public static $cache_time_oid = 259200 ; // cache oid resolver items for 3 days
        public static $cache_time_invites = 86400 ; //minimum interval between invitation mails in sec
        public static $cache_time_groups=90;

        //USER SPECIFIC STUFF

        public static $clientId = '04c16f20-845f-4307-94e8-753afe140bcd';
        public static $password = 'D0yRy92NcmNYAZK0wuvONmY90Sth4Mh8n2wpFWtJUdg=';
        public static $authority = 'https://login.microsoftonline.com/common/';


        public static $tokenCache = 'TOKEN_CACHE';
        public static $isAddin = 'IS_ADDIN';
        public static $apiRoot = 'API_ROOT';
        public static $redirectURI = 'https://o365sync.dog.org/Login.php';
        public static $unifiedAPIResource = 'https://graph.microsoft.com';
        //public static $unifiedAPIEndpoint = 'https://graph.microsoft.com/beta/';
        public static $unifiedAPIEndpoint = 'https://graph.microsoft.com/v1.0/';
        public static $tokenCache = 'TOKEN_CACHE';
        public static $isAddin = 'IS_ADDIN';
        public static $apiRoot = 'API_ROOT';
        // the domain we push users/invites to
        public static $tenantDOMAIN = 'M365x57871887.onmicrosoft.com';
        // tenantADMINmail is the mail handle ( all before @) of the default group(==list) owner
        public static $tenantADMINmail  = 'office';
        // tenantINVITEmail is the mail handle ( all before @) of that will send email to new users
        public static $tenantINVITEmail = 'usermanagemement';     

        public static $inviteMailFooter = ' <br><hr><br>Best Regards , your Organization_Name <br><br><br>Foooter with GDPR and PARAGRAGPHS LIKE YOU HO HAVE TO OBEY TO LAW § 666 LAWNAME:<br><br>ORGNAME<br>CEO: Dr. &Ouml;ver 9000& &Uuml;mlaut  La\szlig;tn&auml;me <br><br>Tel. +1 22 33 44 55<br>Fax +1 111 222 333 4441<br><img src="https://www.orgnameorgorgorg.org/wp-content/themes/newdog/pixs/logo_org.gif" > <br><br> ';

        // public static $reply_to = 'usermanagemement@org.org';

    }
	?>



This project has adopted the [Microsoft Open Source Code of Conduct](https://opensource.microsoft.com/codeofconduct/). For more information, see the [Code of Conduct FAQ](https://opensource.microsoft.com/codeofconduct/faq/) or contact [opencode@microsoft.com](mailto:opencode@microsoft.com) with any additional questions or comments.
