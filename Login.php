<?php



if (!class_exists("Settings"))	 {
  	require_once( dirname(__FILE__) . "/Settings.php");

  }
if (Settings::$debugApp) {

  error_reporting(E_ALL|E_STRICT);
  ini_set("display_errors", 1);
}
if(php_sapi_name() != "cli") {
	session_start();
	}

//require_once("AuthHelper.php");
//require_once("Token.php");
require_once("MS-graph-functions.php");

//get addin value

if (isset($_SESSION[Settings::$isAddin])) {
$isaddin = $_SESSION[Settings::$isAddin];
} else { $isaddin=false;}

//check for authorization code in url parameter
if (isset($_GET["code"])) {
  //use the authorization code to get access token for the unified API
  $token = AuthHelper::getAccessTokenFromCode($_GET["code"]);
  if (isset($token->refreshToken)) {
    $_SESSION[Settings::$tokenCache] = $token->refreshToken;
    if (isset($_GET["redirect"])) {
	 header("Location:".$_GET["redirect"]);
		}else {
     header("Location:Index.php"); }
  }
}
echo '<html>
<head>
  <title>Login</title>
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <script type="text/javascript" src="scripts/jquery-1.10.2.min.js"></script>
  <script type="text/javascript" src="scripts/bootstrap.min.js"></script>
  <script type="text/javascript">
    function login() {
        window.location = "'.AuthHelper::getAuthorizationUrl().'";';
echo '    }
  </script>';

  if ($isaddin) {
 echo ' <script type="text/javascript" src="//appsforoffice.microsoft.com/lib/1/hosted/office.js"></script>
  <script type="text/javascript">
  //initialize Office on each page if add-in
  Office.initialize = function(reason) {
    $(document).ready(function() {

    });
  }
  </script>';
}
echo '</head>
<body>
  <div class="container">
    <div class="page-header page-header-inverted">
      <h1>'.Settings::$AppName.'</h1>
    </div>
    <div class="row">
      <div class="col-sm-12">
        <div class="panel panel-default">
          <div class="panel-body">
            <h3>Login Required</h3>
            <p>'.Settings::$AppName.' is a web application that queries Office 365 to display the modern groups of your groups.
            To query Office 365, you must first login with Office 365 credentials and then grant this application access to query group data.
            OR pre-authorize this application in AD</p>
            <button type="button" name="button" onclick="login()" class="btn btn-primary btn-block">Login with Office 365</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</body>
</html>';
 ?>
