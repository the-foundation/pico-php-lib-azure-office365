<?php
$start_time = microtime(true);


if (!class_exists("Settings"))	 {
  	require_once( dirname(__FILE__) . "/Settings.php");

  }
if (Settings::$debugApp) {

  error_reporting(E_ALL|E_STRICT);
  ini_set("display_errors", 1);
}
if(php_sapi_name() != "cli") {
	session_start();
	}

global $bootmsg;
$bootmsg="";

//require_once("AuthHelper.php");
//require_once("Token.php"); *
require_once("MS-graph-functions.php");


//check for token in session first time in
if (!isset($_SESSION[Settings::$tokenCache])) {
  //set the isadd flag if necessary
  if (isset($_GET["addin"]))
    $_SESSION[Settings::$isAddin] = true;
  else
    $_SESSION[Settings::$isAddin] = false;

  //redirect to login page
  header("Location: Login.php");
  echo '<html><h1>Please login</h1></html>';
} else {
  //get addin value
  if(isset($_SESSION[Settings::$isAddin])) {
  $isaddin = $_SESSION[Settings::$isAddin];
  } else { $isaddin=false;}
  //use the refresh token to get a new access token
  //$token = AuthHelper::getAccessTokenFromRefreshToken($_SESSION[Settings::$tokenCache]);
  //  $targetURL=Settings::$unifiedAPIEndpoint . "me/joinedgroups";

  //perform a REST query for the users modern groups

//  $token = AuthHelper::getDaemonToken();
  $targetURL=Settings::$unifiedAPIEndpoint . "groups";
//  //$targetURL=Settings::$unifiedAPIEndpoint . "users";
//
//  //request = curl_init(Settings::$unifiedAPIEndpoint . "groups");
  echo '<html><head>';


  echo '<script>console.log(\' BOOT:'.$bootmsg.'\');</script>';
  echo '<script>console.log(\' URL '.$targetURL.'\');</script>';

//  echo '<script>console.log(\' TOK: '.$token->accessToken.'\');</script>';
//  echo '<script>console.log(\''.json_encode($token->scope).'\');</script>';

  //$request = curl_init($targetURL);
  //curl_setopt($request, CURLOPT_HTTPHEADER, array(
  //  "Authorization: Bearer " . $token->accessToken,
  //  "Accept: application/json"));
  //curl_setopt($request, CURLOPT_RETURNTRANSFER, true);
  //$response = curl_exec($request);
  //echo '<script>console.log(\' RES: '.$response.'\');</script>';
  ////parse the json into oci_fetch_object
  //$groups = json_decode($response, true);
  $groups=graph_getAllGroups();

  if (isset($groups) && !empty($groups) && !isset($groups["error"]["code"]) ) {
 	   echo '<script>console.log(\' GROUPS LOADED\');</script>';

 /// $apiRoot = $groups["@odata.context"];
 /// $apiRoot = substr($apiRoot, 0, strrpos($apiRoot, "/"));
 /// $_SESSION[Settings::$apiRoot] = $apiRoot;
 } else {
	   echo '<script>console.log(\' GROUPS NOT LOADED\');</script>';

	 }

}
echo '  <title>'.Settings::$AppName.' - HOME</title>
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <script type="text/javascript" src="scripts/jquery-1.10.2.min.js"></script>
  <script type="text/javascript" src="scripts/bootstrap.min.js"></script>
    <style>';
    require_once(Settings::$styleSheetPHPfile);
  echo '</style> ';

 if (isset($isaddin) && $isaddin) {
	 echo '
  <script type="text/javascript" src="//appsforoffice.microsoft.com/lib/1/hosted/office.js"></script>
  <script type="text/javascript">

  //initialize Office on each page if add-in
  Office.initialize = function(reason) {
    $(document).ready(function() {

    });
  }
  </script>';
}
echo '
</head>
<body>
  <div class="maincontainer min85" >';



require_once("Menu.php");

if (isset($_GET["redisStats"])) {
$internal_token=uniqid("synctok");
file_put_contents( "/tmp/stats_token","TOK".$internal_token);
echo '<hr><div class="brightbg round"><center><h3>Cache Info:</h3><br><iframe style="min-width: 85%;min-height: 666px" src="/redis-stats/?StatsToken='.$internal_token.'">';
//require_once("_redis_stats.php");
echo '</iframe></center></div>';
} else {
  echo '<div class="row min85 center brightbg">
      <div class="col-sm-12 center brightbg">
        <table class="table table-striped table-hover">
          <thead>
            <tr>
              <th>Group Name</th>
              <th>Email</th>
              <th>renewedDateTime</th>
            </tr>
          </thead>
          <tbody> ';
          if (isset($groups) && !empty($groups) ) {
           foreach ($groups["value"] as $group) {
			    echo '<script>console.log(\' GROUP:'.json_encode($group).'\');</script>';

		  echo '<tr>
                <td><a href="Detail.php?id='.$group["id"].'">'.$group["displayName"].' </a></td>
                <td><a href="mailto:'.$group["mail"].'">'.$group["mail"].'</a></td>
                <td>'.$group["renewedDateTime"].'</td>


              </tr>';
         }
}
echo '
          </tbody>
        </table>
      </div>';
    echo '</div>';
}

echo ' </div>' ;
echo ' </div>' ;
echo '<hr> <div class="round border1 brightbg" id="renderTime" >  <center>Render time: '.( microtime(true) - $start_time).' seconds   </center></div>';

echo' </body>
</html>' ;

?>
